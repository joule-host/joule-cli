# joule-cli

This repository holds the code for the `joule` command line program.

## Goals 

The goal of [joule.host](https://joule.host) is to make uploads of datasets as 
easy as uploads of code to Gitlab/GitHub.

## Install

Download Joule for your architecture:

* macOS: 
  [32 bits](https://joule-host.gitlab.io/joule-cli/builds/darwin-386/joule.tar.gz),
  **[64 bits](https://joule-host.gitlab.io/joule-cli/builds/darwin-amd64/joule.tar.gz)**
* Linux:
  [32 bits](https://joule-host.gitlab.io/joule-cli/builds/linux-386/joule.tar.gz),
  **[64 bits](https://joule-host.gitlab.io/joule-cli/builds/linux-amd64/joule.tar.gz)**,
  [ARM](https://joule-host.gitlab.io/joule-cli/builds/linux-arm/joule.tar.gz),
  [AArch64](https://joule-host.gitlab.io/joule-cli/builds/linux-arm64/joule.tar.gz)
* Windows:
  [32 bits](https://joule-host.gitlab.io/joule-cli/builds/windows-386/joule.exe.tar.gz),
  **[64 bits](https://joule-host.gitlab.io/joule-cli/builds/windows-amd64/joule.exe.tar.gz)**,
  [ARM](https://joule-host.gitlab.io/joule-cli/builds/windows-arm/joule.exe.tar.gz)

If you are unsure, just pick the 64 bits version for your OS.

On Unix (e.g. on macOS or on Ubuntu), 
you just need to extract the executable 
and put it anywhere in your `$PATH`:

```bash
# On macOS:
curl YOUR_DOWNLOAD_LINK | tar xz -C /usr/local/bin/
# On Debian/Ubuntu:
sudo id && curl YOUR_DOWNLOAD_LINK | sudo tar xz -C /usr/local/bin/
```

## Usage

Ensure the command line utility has the necessary permissions to 
perform dataset creation/uploads on your behalf. 

To authenticate, use the following command:

```bash
joule auth
```

Now you may upload your dataset. Initialise your dataset:

```bash
cd YOUR_DATASET_DIRECTORY
joule init "DATASET_NAME"
```

This command will initialize you folder with a joule configuration 
file (`.joule-spec.yaml`). **Make sure you commit and push this file to your code repository, 
as joule uses this to find your dataset.**
You can then start uploading your dataset using:

```bash
joule push
```

Now, any registered user will be able to download your dataset by using 
`joule pull` in the folder containing your dataset.

## Supported

For now, the tool is pretty basic and only supports basic upload and download
of a dataset using the `.joule-spec.yaml` descriptor file.

## To do

Planned:

- [ ] Support compression during upload/download
- [ ] Handle download of datasets in subdirectories
- [ ] Add a command to register a git hook
- [ ] Add support for large file uploads
- [ ] Add support for million of small files uploads using [archive/tar](https://golang.org/pkg/archive/tar/) 

Would probably be a good idea:

- [ ] Support torrent download descriptors
- [ ] Add Joule to the package managers

