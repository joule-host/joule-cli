Bug report
==========

```bash
joule [write the command you ran here]
```

### What behavior did you expect from this command?

[Write here]

### What happened instead?

[Write here]

### Relevant logs:

```
[Insert relevant output logs here]
```



/label ~bug ~needs-investigation