#!/usr/bin/env bash
set -euo pipefail
BIN_FILE_NAME_PREFIX=$1
PROJECT_DIR=$2
# To get a list of available platforms: go tool dist list
PLATFORMS="darwin/amd64 linux/386 linux/amd64 linux/arm linux/arm64 windows/386 windows/amd64 windows/arm"
for PLATFORM in $PLATFORMS; do
  # Create build directory
  GOOS=${PLATFORM%/*}
  GOARCH=${PLATFORM#*/}
  cd "$PROJECT_DIR"
  FILEPATH="$PROJECT_DIR/builds/${GOOS}-${GOARCH}"
  mkdir -p "$FILEPATH"
  # Build
  BIN_FILE_NAME="$FILEPATH/${BIN_FILE_NAME_PREFIX}"
  if [ "${GOOS}" = "windows" ]; then BIN_FILE_NAME="${BIN_FILE_NAME}.exe"; fi
  CMD="GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags \"-X gitlab.com/joule-host/joule-cli/globals.VERSION=$CI_COMMIT_SHORT_SHA -X gitlab.com/joule-host/joule-cli/globals.BackendHostString=https://api.joule.host -X gitlab.com/joule-host/joule-cli/globals.FrontendHostString=https://joule.host\" -o ${BIN_FILE_NAME}"
  echo "${CMD}"
  eval "$CMD"
  # Allow execution
  if [ "${GOOS}" != "windows" ]; then chmod +x "${BIN_FILE_NAME}"; fi
  # Generate SHA Hash
  sha256sum -b "${BIN_FILE_NAME}" | cut -d ' ' -f1 >"${BIN_FILE_NAME}".sha256
  cd "$(dirname "${BIN_FILE_NAME}")"
  # Make a compressed archive of the binary
  tar -czvf "$(basename "${BIN_FILE_NAME}")".tar.gz "$(basename "${BIN_FILE_NAME}")"
  # Only keep the .tar.gz (keep the .exe on Windows)
  if [ "${GOOS}" != "windows" ]; then rm "${BIN_FILE_NAME}"; fi
done
