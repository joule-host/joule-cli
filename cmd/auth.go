package cmd

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os/exec"
	"runtime"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/api"
	"gitlab.com/joule-host/joule-cli/auth"
	"gitlab.com/joule-host/joule-cli/globals"
)

func init() {
	authCommand.Flags().BoolVarP(&reset, "reset", "r", false,
		"forget keys and retrieve new ones from joule.host")
	authCommand.Flags().BoolVar(&noBrowser, "no-browser", false,
		"do not try to open the web browser automatically and print the authorization link instead")
	authCommand.Flags().BoolVar(&storeUnencrypted, "store-unencrypted", false,
		"store the API key secret on-disk, unencrypted")
	rootCmd.AddCommand(authCommand)
}

var authCommand = &cobra.Command{
	Use:   "auth",
	Short: "Authenticate through Joule.Host",
	Long:  `Use this command to authenticate this command line utility.`,
	RunE:  authorize,
}

var reset bool
var noBrowser bool
var storeUnencrypted bool

// authorize
func authorize(*cobra.Command, []string) error {
	jww.TRACE.Println("Starting new authorization process.")
	if reset {
		auth.ClearKeyFromConfig()
	} else {
		if a := auth.APIKeyFromConfig(); a != nil {
			fmt.Println("An API Key is already saved. " +
				"Use `--reset` to force the creation of a new key.")
			jww.INFO.Println("An API key is already saved.")
			return nil
		}
	}

	// Negotiate Authorization Message
	c := api.NewUnauthenticatedClient()
	res, err := c.R().SetResult(&authMessage{}).Post("/joule-cli/api-key/auth/new")
	if err != nil {
		sentry.CaptureException(err)
		return fmt.Errorf("unable to generate new authorization using Joule API. Error: %w.\n"+
			"Please make sure you are running the latest version of Joule", err)
	}
	authMessage, ok := res.Result().(*authMessage)
	if !ok || len(authMessage.MessageID) == 0 || len(authMessage.MessageRetrievalKey) == 0 {
		err := errors.New("unexpected response received from the Joule API")
		sentry.ConfigureScope(func(s *sentry.Scope) {
			s.SetExtra("able_to_unmarshal", ok)
			s.SetExtra("response", res.String())
		})
		sentry.CaptureException(err)
		return fmt.Errorf("%w. \nPlease make sure you are running the latest version of Joule", err)
	}

	u := globals.AuthServerURL()
	q := u.Query()
	q.Set("id", authMessage.MessageID)
	u.RawQuery = q.Encode()

	// Open Web auth
	if (!noBrowser) && tryOpeningBrowser(u) {
		jww.TRACE.Println("Opened browser.")
		fmt.Println("Please authorize the CLI using the web page opening on your browser.")
	} else {
		jww.TRACE.Println("Displaying authorization URL.")
		fmt.Printf("Please open a web-browser and point it to the following URL: "+
			"\n\n\t%s\n\n", u.String())
	}

	// Function that will ask the server for a response
	request := func() (authMessageContent, error) {
		resp, err := c.R().
			SetBody(authMessage).
			SetResult(&authMessageContent{}).
			Post("/joule-cli/api-key/auth/read")
		if err != nil {
			return authMessageContent{}, err
		}
		if resp.StatusCode() == http.StatusNoContent {
			return authMessageContent{}, nil
		} else if resp.StatusCode() != http.StatusOK {
			return authMessageContent{}, errors.New("an unexpected response code was returned by the Joule API")
		}
		result, ok := resp.Result().(*authMessageContent)
		if !ok {
			return authMessageContent{}, errors.New("unexpected response from the Joule API")
		}
		return *result, nil
	}

	// First wait about 2 seconds for the user to authorize the app
	time.Sleep(2 * time.Second)

	// Check every 3 seconds if a key was added.
	tick := time.Tick(3 * time.Second)
	looping := true
	var a auth.APIKey
	for looping {
		select {
		case <-tick:
			res, err := request()
			if err != nil {
				sentry.CaptureException(err)
				return fmt.Errorf("unexpected error when polling the Joule API for authorization keys. \n"+
					"Error:%w\n"+
					"Please make sure you are running the latest version of Joule", err)
			}
			if len(res.APIKey.Key) > 0 && len(res.APIKey.Secret) > 0 {
				looping = false
				a = auth.APIKey{
					Key:    res.APIKey.Key,
					Secret: res.APIKey.Secret,
				}
				break
			}
		}
	}

	a.SaveToDisk(!storeUnencrypted)
	jww.INFO.Println("Authorization was successful.")
	fmt.Println("Success: Joule is now authorized to use your Joule.Host credentials.")
	return nil
}

type authMessage struct {
	MessageID           string `json:"message_id"`
	MessageRetrievalKey string `json:"message_retrieval_key"`
}

type authMessageContent struct {
	APIKey struct {
		Key    string `json:"key"`
		Secret string `json:"secret"`
	} `json:"api_key"`
}

func tryOpeningBrowser(u *url.URL) bool {
	var err error
	urlString := u.String()

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", urlString).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", urlString).Start()
	case "darwin":
		err = exec.Command("open", urlString).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		return false
	}
	return true
}
