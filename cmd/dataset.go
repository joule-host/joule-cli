package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/joule-host/joule-cli/dataset"
)

func init() {
	// TODO: Enable ForceUpload
	// datasetUploadCommand.Flags().BoolVarP(&dataset.ForceUpload, "force", "f", false,
	//	"force a re-upload of all the files")
	datasetCreateCommand.Flags().BoolVar(&dataset.NoGenerateGitignore, "no-gitignore", false,
		"disable generation of a `.gitignore` file for the dataset")
	datasetDownloadCommand.Flags().BoolVarP(&dataset.OverwriteFiles, "force", "f", false,
		"overwrite local files")
	datasetDeleteCommand.Flags().BoolVar(&dataset.DeleteFromRemote, "remote", false,
		"delete the dataset from cloud storage as well")
	rootCmd.AddCommand(datasetCreateCommand, datasetDeleteCommand, datasetUploadCommand, datasetDownloadCommand)
}

var datasetCreateCommand = &cobra.Command{
	Use:        "init [name]",
	SuggestFor: []string{"new", "create"},
	Short:      "Initialise a new dataset",
	Long: `Initialises a new dataset with an optional given name.
If a name is not provided, the directory name will be used instead.`,
	Args: cobra.MaximumNArgs(1),
	RunE: dataset.CommandCreate,
}

var datasetDeleteCommand = &cobra.Command{
	Use:        "delete",
	SuggestFor: []string{"deinit", "remove"},
	Short:      "Remove a dataset locally and from the server",
	Long:       `Removes a dataset from Joule.Host.`,
	Args:       cobra.MaximumNArgs(1),
	RunE:       dataset.CommandDelete,
}

var datasetUploadCommand = &cobra.Command{
	Use:        "push",
	SuggestFor: []string{"upload", "commit"},
	Short:      "Upload the current dataset",
	Long: `This command pushes a dataset to joule.host.
The dataset has to be initialised with 'joule init' beforehand.`,
	RunE: dataset.UploadDataset,
}

var datasetDownloadCommand = &cobra.Command{
	Use:        "pull",
	SuggestFor: []string{"download", "fetch"},
	Short:      "Download the current dataset",
	Long: `This command pulls a dataset from Joule.Host.
A Joule spec file must be present in the current directory for 
this command to know how to behave.`,
	RunE: dataset.DownloadDataset,
}
