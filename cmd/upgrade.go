package cmd

import (
	"archive/tar"
	"compress/gzip"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"

	"github.com/getsentry/sentry-go"
	"github.com/kardianos/osext"
	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/api"
	"gitlab.com/joule-host/joule-cli/globals"
	"gitlab.com/joule-host/joule-cli/upgrade"
)

func init() {
	rootCmd.AddCommand(upgradeCommand)
}

var upgradeCommand = &cobra.Command{
	Use:        "upgrade",
	SuggestFor: []string{"update"},
	Short:      "Upgrade the Joule CLI",
	Long:       `This command upgrades the joule-cli tool to the latest available version.`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		// Overrides persistent run command of the root command (like the upgrade check here)
	},
	RunE: jouleUpgrade,
}

type upgradeResponse struct {
	Platform    string `json:"platform"`
	SHAHash     string `json:"sha_256"`
	DownloadURL string `json:"url_tar_gz"`
}

func jouleUpgrade(*cobra.Command, []string) error {
	fmt.Println("Checking if an upgrade is available...")
	shouldUpgrade, err := upgrade.CheckForUpdates(true)
	if err != nil {
		return nil
	}
	if !shouldUpgrade {
		fmt.Println("You are already using the latest version.")
		return nil
	}

	// Upgrade
	jww.INFO.Println("Upgrading from version:", globals.VERSION)
	platform := fmt.Sprintf("%s-%s", runtime.GOOS, runtime.GOARCH)
	jww.INFO.Println("Upgrade will be performed for platform", platform)
	sentry.ConfigureScope(func(s *sentry.Scope) {
		s.SetExtra("platform", platform)
	})

	fmt.Println("An upgrade is available. Fetching download details...")
	resp, err := api.NewUnauthenticatedClient().R().
		SetPathParams(map[string]string{"platform": platform}).
		SetResult(&upgradeResponse{}).
		Get("/joule-cli/{platform}")
	if err != nil {
		return err
	}

	result, ok := resp.Result().(*upgradeResponse)
	if !ok || len(result.DownloadURL) == 0 {
		sentry.ConfigureScope(func(s *sentry.Scope) {
			s.SetExtras(map[string]interface{}{
				"response":          resp.String(),
				"able_to_unmarshal": ok,
			})
		})
		err := errors.New("unable to understand the server's response when fetching new version")
		sentry.CaptureException(err)
		return err
	}

	// Download
	fmt.Println("Preparing download...")
	response, err := http.Get(result.DownloadURL)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	gzipReader, _ := gzip.NewReader(response.Body)
	defer gzipReader.Close()

	tarReader := tar.NewReader(gzipReader)
	tarReader.Next()

	file, err := ioutil.TempFile("", "joule_new_version")
	if err != nil {
		sentry.CaptureException(err)
		return err
	}
	defer file.Close()

	// Write to tarReader and Hash function
	fmt.Println("Downloading...")
	hSHA := sha256.New()
	_, err = io.Copy(io.MultiWriter(file, hSHA), tarReader)
	if err != nil {
		sentry.CaptureException(err)
		return err
	}

	// Check Hash
	fmt.Println("Checking hash...")
	hash := fmt.Sprintf("%x", hSHA.Sum(nil))
	if hash != result.SHAHash {
		err := errors.New("the downloaded file did not match the hash we expected")
		sentry.CaptureException(err)
		return fmt.Errorf("%w. \n"+
			"This might happen if a new version of the Joule CLI is ongoing a new deployment.\n"+
			"Please try again", err)
	}

	// Install
	fmt.Println("Installing...")
	if err = file.Chmod(0755); err != nil {
		sentry.CaptureException(err)
		return err
	}
	file.Close() // We don't need the file anymore

	currentExecutable, _ := osext.Executable()
	err = os.Rename(file.Name(), currentExecutable)
	if err != nil {
		if errors.Is(err, os.ErrPermission) {
			return errors.New("unable to upgrade Joule, permission to upgrade the old binary was denied.\n" +
				"You might want to try running this command with `sudo` instead")
		}
		sentry.CaptureException(err)
		return err
	}

	jww.INFO.Println("Successfully upgraded.")
	fmt.Println("Upgrade done. You now have the latest version of the Joule CLI.")

	return nil
}
