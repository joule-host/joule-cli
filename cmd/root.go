package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/globals"
	"gitlab.com/joule-host/joule-cli/upgrade"
)

func init() {
	rootCmd.Flags().BoolVar(&displayVersionInfo, "version", false,
		"displays version info")
	rootCmd.PersistentFlags().BoolVarP(&enableVerbose, "verbose", "v", false,
		"enable TRACE-level logs")
}

var displayVersionInfo = false
var enableVerbose = false

var rootCmd = &cobra.Command{
	Use:   "joule",
	Short: "joule is responsible for storing and retrieving datasets",
	Long: `A clean and simple dataset manager.

Complete documentation will be available at https://joule.host`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) { // Note: overridden in cmd/upgrade.go
		_, _ = upgrade.CheckForUpdates(false)
	},

	Run: func(cmd *cobra.Command, args []string) {
		if enableVerbose {
			jww.SetStdoutThreshold(jww.LevelTrace)
		}
		if displayVersionInfo {
			fmt.Printf("Joule Version %s\n", globals.VERSION)
		} else {
			_ = cmd.Help()
			return
		}
	},
}

// Execute the Cobra root command.
// Errors are handled by displaying them and Exiting with 1.
func Execute() {
	fmt.Println("\t┌──────────────────────────────────────────────────────────┐")
	fmt.Println("\t│ You are running an alpha version of the Joule CLI.       │")
	fmt.Println("\t│ It might contain bugs, and should be updated frequently. │")
	fmt.Println("\t│ To report any issue you encounter, please visit:         │")
	fmt.Println("\t│    https://gitlab.com/joule-host/joule-cli/issues/new    │")
	fmt.Println("\t└──────────────────────────────────────────────────────────┘")
	if err := rootCmd.Execute(); err != nil {
		if jErr, ok := err.(*globals.JouleError); ok {
			fmt.Printf("Unable to proceed: %s.\n", jErr.Error())
			if len(jErr.HelpMessage) > 0 {
				fmt.Println(jErr.HelpMessage)
			}
		} else {
			fmt.Printf("Error: %s.\n", err.Error())
		}
		os.Exit(1)
	}
}
