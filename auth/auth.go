package auth

import (
	"errors"
	"fmt"
	"log"

	"github.com/getsentry/sentry-go"
	"github.com/spf13/viper"
	"github.com/zalando/go-keyring"

	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/config"
)

const (
	keyringServiceName = "joule-cli"
	apiKeyConfigKey    = "api-key"
	apiSecretConfigKey = "api-secret"
)

// APIKeyFromConfig Reads the configuration files and the keyring
// for an API key and its associated secret.
func APIKeyFromConfig() *APIKey {
	k := viper.GetString(apiKeyConfigKey)
	if len(k) == 0 {
		return nil
	}
	sentry.ConfigureScope(func(scope *sentry.Scope) {
		scope.SetUser(sentry.User{
			ID: k,
		})
	})
	sentry.AddBreadcrumb(&sentry.Breadcrumb{
		Category: "auth",
		Message:  "Read Authentication Key from config",
		Level:    sentry.LevelInfo,
	})
	s := viper.GetString(apiSecretConfigKey)
	if len(s) > 0 {
		// Secret was stored unencrypted on disk
		jww.TRACE.Printf("Secret for key '%s' is stored unencrypted.", k)
		return &APIKey{
			Key:    k,
			Secret: s,
		}
	}
	// Read secret from Keyring
	return apiKeyFromKeychain(k)
}

// apiKeyFromKeychain Retrieve the secret associated to API key `k`.
// Returns `nil` if the secret is not found in the keyring.
func apiKeyFromKeychain(k string) *APIKey {
	s, err := keyring.Get(keyringServiceName, k)
	if err != nil {
		if errors.Is(err, keyring.ErrNotFound) {
			return nil
		}
		log.Fatalln(err)
	}
	return &APIKey{
		Key:    k,
		Secret: s,
	}
}

// An APIKey to connect to Joule.Host
type APIKey struct {
	Key    string `json:"key"`
	Secret string `json:"secret"`
}

// SaveToDisk Save the API key to the disk
func (a *APIKey) SaveToDisk(useKeyring bool) {
	if useKeyring {
		// Remove any unencrypted API Secret config
		config.Set(apiSecretConfigKey, "")
		// save secret to keyring
		err := keyring.Set("joule-cli", a.Key, a.Secret)
		if err != nil {
			if errors.Is(err, keyring.ErrUnsupportedPlatform) {
				jww.CRITICAL.Fatalln("This platform does not support the keyring.\n" +
					"You can save your API key using the `--store-unencrypted` option.")
			} else {
				err = fmt.Errorf("unable to save the key secret in your keyring: %w", err)
				jww.CRITICAL.Fatalln("Unable to save the key secret in your keyring:", err, "\n"+
					"Please note that you can also save your API key secret "+
					"unencrypted using the `--store-unencrypted` option.")
			}
		}
	} else {
		jww.WARN.Println("Saving the API Key's secret on-disk, unencrypted")
		config.Set(apiSecretConfigKey, a.Secret)
	}
	// Save to config
	config.Set(apiKeyConfigKey, a.Key)
	done := config.Save()
	<-done
}

// ClearKeyFromConfig Reset any saved API key
func ClearKeyFromConfig() {
	k := viper.GetString(apiKeyConfigKey)
	if len(k) > 0 {
		config.Set(apiKeyConfigKey, "")
		config.Set(apiSecretConfigKey, "")
		done := config.Save()
		errKeyring := keyring.Delete("joule-cli", k)
		errConf, ok := <-done
		if ok && errConf == nil && errKeyring == nil {
			fmt.Println("Removed API key from configuration.")
			jww.INFO.Printf("Removed API key '%s' from disk.\n", k)
		} else { // configuration was not cleared
			jww.WARN.Printf("Unable to remove API Key '%s' configuration.\n", k)
		}
	}
}
