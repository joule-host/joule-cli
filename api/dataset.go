package api

import (
	"context"
	"errors"
	"strconv"

	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/globals"
)

// DatasetFile How the Joule API represents
// a file belonging to a Dataset.
type DatasetFile struct {
	Path        string `json:"path"`
	Size        uint   `json:"size"`
	ContentType string `json:"content_type"`
	MD5Hash     string `json:"md5_hash"`
	SHA1Hash    string `json:"sha1_hash"`
}

// DatasetFilesResponse An answer from the Joule API returning a list of Files in a dataset.
type DatasetFilesResponse struct {
	Files []DatasetFile `json:"files"`
	Next  struct {
		StartFrom uint `json:"start_from"`
	} `json:"next"`
}

// GetDatasetFilesAsync Fetches batches of files from the API, forwarding them in batch through the channel.
func GetDatasetFilesAsync(ctx context.Context, datasetID string) (<-chan []DatasetFile, <-chan error) {
	out := make(chan []DatasetFile, 3) // Buffered to make requests ahead
	errCh := make(chan error, 1)
	go func() {
		defer close(errCh)
		defer close(out)
		var startFrom uint
		for {
			response, err := getDatasetFiles(datasetID, startFrom)
			if err != nil {
				errCh <- err
				return
			}
			select {
			case <-ctx.Done():
				jww.INFO.Println("The context was canceled.")
				return
			case out <- response.Files:
			}
			if response.Next.StartFrom > startFrom {
				startFrom = response.Next.StartFrom
			} else {
				return
			}
		}
	}()
	return out, errCh
}

// getDatasetFiles Return the files available for a given dataset.
func getDatasetFiles(datasetID string, startFrom uint) (*DatasetFilesResponse, error) {
	resp, err := NewUnauthenticatedClient().R().
		SetPathParams(map[string]string{
			"dataset_id": datasetID,
		}).
		SetQueryParams(map[string]string{
			"start_from": strconv.Itoa(int(startFrom)),
		}).
		SetResult(&DatasetFilesResponse{}).
		Get("/dataset/{dataset_id}/files")
	if err != nil {
		return nil, errors.New("unable to retrieve dataset: " + err.Error())
	}

	datasetFiles, ok := resp.Result().(*DatasetFilesResponse)
	if !ok || datasetFiles == nil {
		err = errors.New("unable to understand server's response")
	}

	return datasetFiles, err
}

// A DatasetFileChunk is a byte-range of a DatasetFile.
type DatasetFileChunk struct {
	FilePath       string `json:"path"`
	ByteRangeStart uint64 `json:"byte_range_start"`
	ByteRangeEnd   uint64 `json:"byte_range_end"`
}

// A DatasetChunk is a chunk of dataset and contains multiple DatasetFileChunks.
type DatasetChunk struct {
	DownloadLinkString string             `json:"download_url"`
	DownloadHeaders    map[string]string  `json:"download_headers"`
	ChunkSize          uint64             `json:"chunk_size"`
	ChunkSHA1          string             `json:"chunk_sha1"`
	Files              []DatasetFileChunk `json:"files"`
}

// DatasetChunksResponse A response from the Joule.Host API listing chunks.
type DatasetChunksResponse struct {
	Chunks []DatasetChunk `json:"chunks"`
	Next   struct {
		StartFrom uint `json:"start_from"`
	} `json:"next"`
}

// GetDatasetChunksAsync Fetches batches of chunks from the API, forwarding them in batch through the channel.
func GetDatasetChunksAsync(ctx context.Context, datasetID string) (<-chan []DatasetChunk, <-chan error) {
	out := make(chan []DatasetChunk, 3) // Buffered to make requests ahead
	errCh := make(chan error, 1)
	go func() {
		defer close(errCh)
		defer close(out)
		var startFrom uint
		for {
			response, err := getDatasetChunks(datasetID, startFrom)
			if err != nil {
				errCh <- err
				return
			}
			select {
			case <-ctx.Done():
				jww.INFO.Println("The context was canceled.")
				return
			case out <- response.Chunks:
			}
			if response.Next.StartFrom > startFrom {
				startFrom = response.Next.StartFrom
			} else {
				return
			}
		}
	}()
	return out, errCh
}

// getDatasetChunks Return the chunks available for a given dataset.
func getDatasetChunks(datasetID string, startFrom uint) (*DatasetChunksResponse, error) {
	resp, err := NewUnauthenticatedClient().R().
		SetPathParams(map[string]string{
			"dataset_id": datasetID,
		}).
		SetQueryParams(map[string]string{
			"protocol":   globals.JouleProtocolVersion,
			"start_from": strconv.Itoa(int(startFrom)),
		}).
		SetResult(&DatasetChunksResponse{}).
		Get("/dataset/{dataset_id}/chunks")
	if err != nil {
		return nil, errors.New("unable to retrieve dataset: " + err.Error())
	}

	// TODO: switch response code
	chunksResponse, ok := resp.Result().(*DatasetChunksResponse)
	if !ok || chunksResponse == nil {
		return nil, errors.New("unable to understand server's response")
	}
	return chunksResponse, nil
}
