package api

import (
	"errors"
	"fmt"
	"sync"

	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/auth"

	"github.com/go-resty/resty/v2"

	"gitlab.com/joule-host/joule-cli/globals"
)

var client *resty.Client = nil
var clientLock = &sync.Mutex{}

var errorNoAuth = errors.New("no api key authorized")

func authHeaders() (map[string]string, error) {
	k := auth.APIKeyFromConfig()
	if k == nil {
		return nil, errorNoAuth
	}
	return map[string]string{
		"Key":    k.Key,
		"Secret": k.Secret,
	}, nil
}

// NewUnauthenticatedClient Create and return a new client to the API
// to which no Authentication headers will be attached.
func NewUnauthenticatedClient() *resty.Client {
	client := resty.New()

	// Set base URL
	client.SetHostURL(globals.BackendHost.String())
	client.SetHeader("User-Agent", fmt.Sprintf("Joule-CLI/%s", globals.VERSION))

	client.SetRetryCount(2)
	return client
}

// NewClient Create and return a new client to the API
// to which Authentication headers will be attached.
func NewClient() (*resty.Client, error) {
	client := NewUnauthenticatedClient()
	client.SetHostURL(globals.APIURL().String())

	// Set API Key
	authHeaders, err := authHeaders()
	if err != nil {
		return nil, err
	}
	client.SetHeaders(authHeaders)

	return client, nil
}

// GlobalClient Lazily initializes a new Resty GlobalClient. Can be called concurrently.
func GlobalClient() *resty.Client {
	clientLock.Lock()
	defer clientLock.Unlock()
	if client == nil {
		jww.INFO.Println("Instantiating a new API client.")
		var err error
		client, err = NewClient()
		if err != nil {
			if err == errorNoAuth {
				jww.ERROR.Fatalln("You first need to be log-in to perform this action. " +
					"Please use `joule auth` to log-in using the CLI.")
			} else {
				jww.ERROR.Fatalln("Unable to create a new client for API. Error:", err)
			}
		}
	}
	return client
}

// A JouleErrorResponse is a server response from the
// Joule API when an error occurred.
type JouleErrorResponse struct {
	Error            string              `json:"error"`
	ValidationErrors map[string][]string `json:"field_error"`
}
