package api

import (
	"errors"
	"fmt"
	"strings"

	"github.com/getsentry/sentry-go"

	"gitlab.com/joule-host/joule-cli/globals"
)

func newJouleErrorS(error, uiMessage string) *globals.JouleError {
	return NewJouleError(errors.New(error), uiMessage)
}

// NewJouleError Create a new Joule Error from an error.
func NewJouleError(err error, uiMessageFormat string, uiMessageElements ...interface{}) *globals.JouleError {
	return &globals.JouleError{
		HelpMessage: fmt.Errorf(uiMessageFormat, uiMessageElements...).Error(),
		Err:         err,
	}
}

// UnhandledErrorFromError Wraps an error and asks users to report the error on GitLab.
func UnhandledErrorFromError(err error) *globals.JouleError {
	if err == nil {
		err = errUnspecified
	}
	sentry.CaptureException(err)
	return NewJouleError(err,
		"An error happened, but Joule was not programmed to handle it.\n"+
			"The error was: %w\n"+
			"We would be very grateful if you could report this error on: \n"+
			"\thttps://gitlab.com/joule-host/joule-cli/issues/new\n"+
			"or send an email to:\n"+
			"\tincoming+joule-host-joule-cli-14070521-issue-@incoming.gitlab.com\n", err)
}

// UnexpectedReturnCodeError An error when the
// return code cannot be handled by the CLI.
type UnexpectedReturnCodeError struct {
	Endpoint string
	Method   string
	Code     int
}

func (e *UnexpectedReturnCodeError) Error() string {
	return fmt.Sprintf("%s on '%s' received an unexpected return code %d",
		strings.ToUpper(e.Method), e.Endpoint, e.Code)
}

// Errors
var (
	errUnspecified                = errors.New("(no specific error specified)")
	ErrDatasetDoesNotExistAnymore = newJouleErrorS(
		"dataset does not exists",
		"The server reported that the dataset does not exist anymore.\n"+
			"It might have been removed since the beginning of the operation.",
	)
	ErrInternalServer = newJouleErrorS(
		"server internal error",
		"It appears the server is currently experiencing technical issues, "+
			"please try again later.",
	)
	ErrUnauthorized = newJouleErrorS(
		"api key is not authorized to perform this action",
		"The API key is not authorized to perform this action. "+
			"Please use `joule auth --reset` to negotiate a new key.",
	)
	ErrDatasetHasNewerUpload = newJouleErrorS(
		"dataset has another upload ongoing",
		"A new upload was started for this same dataset.\n"+
			"Please stop the new upload, or start again.",
	)
	ErrDatasetUndergoingVerification = newJouleErrorS(
		"dataset is undergoing upload verification",
		"This dataset is currently undergoing verification by the server.\n"+
			"Please wait, or force a new verification by re-uploading first.",
	)
)
