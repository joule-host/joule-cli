package globals

import (
	"errors"
	"fmt"
	"os"

	jww "github.com/spf13/jwalterweatherman"
)

// ErrDatasetDoesNotExist Thrown when a dataset does not exist.
var ErrDatasetDoesNotExist = &JouleError{
	Err:         errors.New("the dataset does not exist on the server"),
	HelpMessage: "Please create it by using `joule init`.",
}

// ErrFilesDoNotExistLocally Thrown when files expected to exist don't exist.
var ErrFilesDoNotExistLocally = &JouleError{
	Err:         errors.New("the uploaded dataset contains files that were removed locally"),
	HelpMessage: "Please put the file back or remove the dataset from the server before re-uploading it.",
}

// ErrUnknownProtocolVersion Thrown when Joule cannot communicate with
// the API because their protocol versions don't match.
var ErrUnknownProtocolVersion = &JouleError{
	Err:         errors.New("the server returned an unknown protocol version"),
	HelpMessage: "Please update this tool using `joule upgrade` and try again",
}

// ErrDatasetUploadComplete Thrown when an upload is requested
// on a dataset whose upload is already complete.
var ErrDatasetUploadComplete = &JouleError{
	Err:         errors.New("the dataset was already uploaded or is still undergoing verification"),
	HelpMessage: "Please create a new one if you want to upload a new version.",
}

// A JouleError is an error meant to be displayed to the user
// at some point if it stays unhandled.
type JouleError struct {
	Err         error  // The actual error
	HelpMessage string // A user-friendly help message corresponding to the error
}

func (e *JouleError) Error() string {
	return e.Err.Error()
}

func (e *JouleError) Unwrap() error {
	return e.Err
}

// Println Print an error to the ERROR level of the logger, and display its UI Message.
func (e *JouleError) Println() {
	jww.ERROR.Println(e.Error())
	fmt.Println(e.HelpMessage)
}

// Fatalln Display underlying error and exit with a 1 error code.
func (e *JouleError) Fatalln() {
	e.Println()
	os.Exit(1)
}

// Panicln Display underlying error and panic.
func (e *JouleError) Panicln() {
	jww.ERROR.Panicln(e.Error())
}
