package globals

import (
	"net/url"

	jww "github.com/spf13/jwalterweatherman"
)

// UnsetVersion The Default version value when it is unset
const UnsetVersion = "UNSET"

// JouleProtocolVersion The version of the Joule Host protocol
// Used to ensure we are able to upload
const JouleProtocolVersion = "1.0"

// VERSION of the current build (set at build time)
var VERSION = UnsetVersion

// BackendHostString The URL to the API (set at build time).
var BackendHostString = "http://localhost:8080"

// FrontendHostString The URL to the Front end (set at build time).
var FrontendHostString = "http://localhost:3000"

// RunningDevBuild Is True if the build is a developer build.
var RunningDevBuild bool

// BackendHost The URL of the backend.
var BackendHost, _ = url.Parse(BackendHostString)

// frontendHost The URL of the frontend.
var frontendHost, _ = url.Parse(FrontendHostString)
var authRelativePath, _ = url.Parse("/joule-cli/auth")
var cliRelativePath, _ = url.Parse("/cli")

func init() {
	//noinspection GoBoolExpressions
	RunningDevBuild = VERSION == UnsetVersion
	if RunningDevBuild {
		jww.SetStdoutThreshold(jww.LevelTrace)
		jww.SetLogThreshold(jww.LevelTrace)
	}
}

// AuthServerURL Return a new URL pointer to the Authorization web invite.
func AuthServerURL() *url.URL {
	return frontendHost.ResolveReference(authRelativePath)
}

// APIURL Return a new URL pointer to the CLI endpoint root.
func APIURL() *url.URL {
	return BackendHost.ResolveReference(cliRelativePath)
}
