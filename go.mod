module gitlab.com/joule-host/joule-cli

go 1.14

require (
	github.com/OpenPeeDeeP/xdg v0.2.0
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964
	github.com/getsentry/sentry-go v0.5.1
	github.com/go-resty/resty/v2 v2.0.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/google/go-cmp v0.4.0
	github.com/google/uuid v1.1.1
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/karrick/godirwalk v1.10.12
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.0.0
	github.com/spf13/viper v1.3.2
	github.com/vbauerster/mpb/v4 v4.9.4
	github.com/zalando/go-keyring v0.0.0-20200121091418-667557018717
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
)
