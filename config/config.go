package config

import (
	"errors"
	"os"
	"path/filepath"

	"github.com/OpenPeeDeeP/xdg"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"

	"gitlab.com/joule-host/joule-cli/globals"
)

const (
	configName = "joule.config"
	configExt  = "yaml"

	// JouleConfigFilename The name of Joule CLI's configuration file.
	JouleConfigFilename = configName + "." + configExt
	// ConfigVersion The version of Joule CLI's configuration file
	configVersion = 1
)

// paths An object that holds all the OS-specific
// standard paths like the Configuration path.
var paths *xdg.XDG

func init() {
	if globals.RunningDevBuild {
		paths = xdg.New("host.joule", "joule-cli-dev")
	} else {
		paths = xdg.New("host.joule", "joule-cli")
	}
	viper.SetConfigName(configName)
	viper.SetConfigType(configExt)
	viper.AddConfigPath(".")
	viper.AddConfigPath(paths.ConfigHome())

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			viper.Set("version", configVersion)
			viper.SetConfigFile(filepath.Join(paths.ConfigHome(), JouleConfigFilename))
			err = os.MkdirAll(paths.ConfigHome(), 0700)
			if err != nil {
				jww.ERROR.Panicln(err)
			}
			err = viper.SafeWriteConfig()
			if err != nil {
				if os.IsNotExist(err) {
					_ = viper.WriteConfig()
				} else {
					jww.ERROR.Panicln(err)
				}
			}
		} else {
			// Config file was found but another error was produced
			jww.ERROR.Panicln("Error reading config file:", err)
		}
	}

	// Config file found and successfully parsed
}

// Set Saves value `v` for key `k` in the current global configuration.
// Returns a channel that either returns an error or is closed
// when the saving on-disk is done.
func Set(k string, v interface{}) {
	viper.Set(k, v)
}

// Save Write viper configuration to disk
func Save() <-chan error {
	done := make(chan error)
	go func() {
		defer close(done)
		if len(viper.ConfigFileUsed()) == 0 {
			err := errors.New("no config file path set")
			done <- err
			return
		}
		err := viper.WriteConfig()
		if err != nil {
			done <- err
		}
	}()
	return done
}
