package main

import (
	"fmt"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/cmd"
	_ "gitlab.com/joule-host/joule-cli/config"
	"gitlab.com/joule-host/joule-cli/globals"
)

func main() {
	if !globals.RunningDevBuild {
		err := sentry.Init(sentry.ClientOptions{
			Dsn:              "https://3c2a91252985415fb4d15ac34ca7a721@sentry.io/5185558",
			AttachStacktrace: true,
			Release:          globals.VERSION,
			ServerName:       "joule-cli.local", // This is to avoid recording user's hostnames
			Environment:      "prod",
		})
		if err != nil {
			jww.ERROR.Fatalln("sentry.Init:", err)
		}
		// Recover from panic and flush buffered events before the program terminates.
		defer func() {
			err := recover()
			if err != nil {
				sentry.CurrentHub().Recover(err)
				fmt.Println("Joule crashed. Error:", err)
				fmt.Println("This is embarrassing... ಠ_ಠ\n" +
					"\tI'm sending an anonymous report to the Joule team...")
				sentry.Flush(5 * time.Second)
				fmt.Println("Done. Sorry for the inconvenience")
				os.Exit(1)
			} else {
				sentry.Flush(2 * time.Second)
			}
		}()
	}

	cmd.Execute()
}
