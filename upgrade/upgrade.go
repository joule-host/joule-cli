package upgrade

import (
	"errors"
	"fmt"
	"log"

	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/api"
	"gitlab.com/joule-host/joule-cli/globals"
)

type updateResult struct {
	Version string `json:"version"`
}

// CheckForUpdates If an update is available on the server
func CheckForUpdates(quiet bool) (canUpgrade bool, err error) {
	if globals.RunningDevBuild {
		jww.WARN.Println("You are running in DEV mode. Updates are not checked.")
		return
	}
	resp, err := api.NewUnauthenticatedClient().R().
		SetResult(&updateResult{}).
		Get("/joule-cli")
	if err != nil {
		log.Println("Unable to retrieve latest version. Error:", err)
		return
	}

	result, ok := resp.Result().(*updateResult)
	if !ok || len(result.Version) == 0 {
		err = errors.New("unable to understand the server's response when fetching new version")
		log.Println("Error:", err)
		return
	}

	if result.Version != globals.VERSION {
		canUpgrade = true
		if !quiet {
			fmt.Printf("\nNote: A new version of joule is available. Please upgrade by using `joule upgrade`\n\n")
		}
	}
	return
}
