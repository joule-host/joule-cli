package dataset

import (
	"bytes"
	"context"
	"sync"
	"testing"

	"gitlab.com/joule-host/joule-cli/api"
)

func TestBuildFilesToDownloadMapping(t *testing.T) {
	tests := []struct {
		name      string
		fileSizes [][2]uint
	}{
		{name: "NoInputFile"},
		{
			"NormalFiles1",
			[][2]uint{
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{6 * MB, 8 * MB},
				{1 * MB, 2 * MB},
				{10 * MB, 10 * MB},
				{15 * MB, 0},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, availableFiles, chunks, _ := buildFilesAndChunks(t, test.fileSizes)
			OverwriteFiles = true
			fileMap, _, err := buildFileToDownloadMap(availableFiles, chunks)
			if err != nil {
				t.Fatalf("Error building file download map: %s", err.Error())
			}

			// Tests
			if len(fileMap) != len(availableFiles) {
				t.Error("The file map should have the size of the available files size.")
			}
		})
	}
}

func TestFileWriter(t *testing.T) {
	tests := []struct {
		name      string
		fileSizes [][2]uint
	}{
		{name: "NoInputFile"},
		{
			"NormalFiles1",
			[][2]uint{
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{6 * MB, 8 * MB},
				{1 * MB, 2 * MB},
				{10 * MB, 10 * MB},
				{15 * MB, 0},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			expectedFiles, availableFiles, chunksInfo, downloadedChunks := buildFilesAndChunks(t, test.fileSizes)
			filesToDownload, _, err := buildFileToDownloadMap(availableFiles, chunksInfo)
			if err != nil {
				t.Fatalf("Error building file download map: %s", err.Error())
			}

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()
			OverwriteFiles = true // Allow removal of files
			downloadedChunksChan := make(chan *downloadedChunk)
			go func() {
				defer close(downloadedChunksChan)
				for _, downloadedChunk := range downloadedChunks {
					select {
					case downloadedChunksChan <- downloadedChunk:
					case <-ctx.Done():
						return
					}
				}
			}()

			wg := &sync.WaitGroup{}
			errChan := make(chan string)
			defer close(errChan)
			go func() {
				for err := range errChan {
					t.Error("Received error from fileWriterWorker:", err)
				}
			}()
			for i := uint(0); i < nbFileWriters; i++ {
				wg.Add(1)
				go fileWriterWorker(ctx, cancel, wg, downloadedChunksChan, filesToDownload, errChan)
			}
			wg.Wait()

			// Check that files were successfully written
			for _, expectedFile := range expectedFiles {
				if !fileExists(expectedFile.Path) {
					t.Errorf("File '%s' was supposed to be downloaded but it is not downloaded locally.",
						expectedFile.Path,
					)
				}
				if !expectedFile.isAvailableLocally() {
					t.Errorf("File '%s' was supposed to be downloaded but it is not the same locally.",
						expectedFile.Path,
					)
				}
			}
		})
	}
}

// buildFilesAndChunks Mock the responses from the API when the chunks and available files are requested.
func buildFilesAndChunks(t *testing.T, fileSizes [][2]uint) ([]*fileUploadInfo, []api.DatasetFile, []api.DatasetChunk, []*downloadedChunk) {
	// Generate random files
	inputPaths := mustGenerateRandomFiles(t, fileSizes...)

	// Generate file info structs (compute size/hashes, and so on...)
	filesIn, err := createFileRequestSync(inputPaths)
	if err != nil {
		t.Fatalf("Unable to create file request: %s", err)
	}

	// Chunk files
	chunks := buildChunksSync(t, 1, filesIn)

	// Make available files
	availableFiles := make([]api.DatasetFile, 0)
	for _, file := range filesIn {
		availableFiles = append(availableFiles, api.DatasetFile{
			Path:        file.Path,
			Size:        file.Size,
			ContentType: file.ContentType,
			MD5Hash:     file.MD5Hash,
			SHA1Hash:    file.SHA1Hash,
		})
	}

	// Make chunks
	chunksToDownload := make([]api.DatasetChunk, 0)
	downloadedChunks := make([]*downloadedChunk, 0)
	for chunkIdx, chunk := range chunks {
		fileChunks := make([]api.DatasetFileChunk, 0)
		for _, file := range chunk.Info.Files {
			fileChunks = append(fileChunks, api.DatasetFileChunk{
				FilePath:       file.Path,
				ByteRangeStart: uint64(file.ByteRange.Start),
				ByteRangeEnd:   uint64(file.ByteRange.End),
			})
		}
		datasetChunk := api.DatasetChunk{
			ChunkSize: uint64(chunk.Info.Size),
			ChunkSHA1: chunk.Info.SHA1Hash,
			Files:     fileChunks,
		}
		chunksToDownload = append(chunksToDownload, datasetChunk)
		downloadedChunks = append(downloadedChunks, &downloadedChunk{
			indexedChunk: indexedChunk{
				DatasetChunk: datasetChunk,
				index:        chunkIdx,
			},
			buf: *bytes.NewBuffer(chunk.bytes),
		})
	}

	// Make downloaded chunks

	return filesIn, availableFiles, chunksToDownload, downloadedChunks
}
