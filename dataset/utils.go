package dataset

import (
	"github.com/vbauerster/mpb/v4"
	"github.com/vbauerster/mpb/v4/decor"
)

func createProgressBar(total int64) (*mpb.Progress, *mpb.Bar) {
	p := mpb.New(mpb.WithWidth(20))
	bar := p.AddBar(total,
		mpb.PrependDecorators(
			decor.NewPercentage("%.1f"),
			decor.OnComplete(decor.Spinner(nil, decor.WCSyncSpace), ""),
		),
		mpb.AppendDecorators(
			decor.Counters(decor.UnitKB, "% .2f / % .2f"),
			decor.Name(" — ETA: "),
			decor.OnComplete(decor.AverageETA(decor.ET_STYLE_GO), "Done ✓ "),
		),
	)
	return p, bar
}
