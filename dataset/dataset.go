package dataset

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/getsentry/sentry-go"
	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/joule-host/joule-cli/api"
	"gitlab.com/joule-host/joule-cli/globals"
)

// API interaction

type datasetResponse struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

type datasetCreationResponse struct {
	Message string          `json:"message"`
	Dataset datasetResponse `json:"dataset"`
}

// Flags
var (
	// NoGenerateGitignore When set to True, prevents .gitignore generation
	NoGenerateGitignore bool
	// DeleteFromRemote If True, CommandDelete will attempt
	// to remove the dataset from Joule.Host as well.
	DeleteFromRemote bool
)

// Commands

// CommandCreate Create a dataset with the given name.
func CommandCreate(_ *cobra.Command, args []string) error {
	var name string
	if len(args) > 0 {
		name = strings.Join(args, " ")
	} else {
		// Use directory name as dataset name
		cwd, err := os.Getwd()
		if err != nil {
			return err
		}
		name = strings.Title(filepath.Base(cwd))
		if len(name) == 0 || name == "." {
			jww.ERROR.Fatalln("Cannot determine name from current directory. " +
				"Please provide a name to the `init` command.")
		}
	}
	if datasetExists(nil) {
		fmt.Println("The dataset already exists on the server. No action was taken.")
		return nil
	}
	return createNewDataset(name)
}

// CommandDelete Delete the dataset represented by the current configuration
func CommandDelete(_ *cobra.Command, _ []string) error {
	if !datasetExists(nil) {
		jww.TRACE.Println("Skipping deletion from command: no dataset is referenced locally.")
		fmt.Println("The dataset was already removed.")
		return nil
	}
	if Type() == typeJoule && DeleteFromRemote {
		jErr := deleteDatasetFromServer(ID())
		if jErr != nil {
			return jErr
		}
	}
	err := removeDatasetSpec()
	if err != nil {
		return api.UnhandledErrorFromError(err)
	}
	fmt.Println("Deletion successful.")
	return nil
}

// Functions

// datasetExists Returns true if the dataset is defined by the local
// configuration file and exists on the server.
// WARNING: this function overrides the local configuration file to remove any remaining
func datasetExists(datasetID *string) bool {
	var dID string
	if datasetID == nil {
		dID = ID()
	} else {
		dID = *datasetID
	}
	if len(dID) == 0 {
		// The dataset is not registered locally
		return false
	}

	// dataset.DatasetID() is set
	// Query the server to make sure it exists
	req := api.NewUnauthenticatedClient().R()
	r, err := req.SetPathParams(map[string]string{
		"id": dID,
	}).Get("/dataset/{id}")
	if err != nil {
		jww.ERROR.Fatalln("Unable to check if dataset exists. Error:", err)
	}
	// Use the status code to determine if the dataset exists.
	var res bool
	switch r.StatusCode() {
	case http.StatusOK:
		res = true
	case http.StatusUnauthorized:
		api.ErrUnauthorized.Fatalln()
	case http.StatusBadRequest:
		// The ID is invalid
		fallthrough
	default:
		// FIXME: If the dataset does not belong to the current user, this will also
		// 		  overwrite the current ID. This might be an expected behavior.
		_ = removeDatasetSpec()
		res = false
	}

	return res
}

func createNewDataset(name string) error {
	client := api.GlobalClient()
	req := client.R()
	req.SetBody(map[string]interface{}{"name": name})
	req.SetResult(datasetCreationResponse{}).SetError(&api.JouleErrorResponse{})
	r, err := req.Post("/dataset")
	if err != nil {
		jww.ERROR.Println("Unable to create new dataset. Error:", err)
		return err
	}
	res := r.Result().(*datasetCreationResponse)
	err = nil // Was already nil
	switch r.StatusCode() {
	case http.StatusCreated:
		done := setDataset(res.Dataset.ID, typeJoule)
		fmt.Println("Dataset creation successful.")
		<-done
	case http.StatusUnauthorized:
		api.ErrUnauthorized.Fatalln()
	case http.StatusBadRequest:
		err = errors.New("invalid parameters for new dataset. " +
			"This is a bug, please make sure you are running the latest version of Joule and try again")
	default:
		if r.IsSuccess() {
			sentry.CaptureMessage("unexpected 2XX return code from API")
			fmt.Println("Dataset creation successful, but the code returned by the API is unexpected. " +
				"Please make sure you are running the last version of Joule.")
		} else {
			sentry.CaptureException(fmt.Errorf("unhandled return code from API: %d", r.StatusCode()))
			err = fmt.Errorf("dataset creation failed with an unexpected code: %d.\n"+
				"Make sure you are running the last version of joule.\n"+
				"Result:\n%+v\n", r.StatusCode(), res)
		}
	}
	if err == nil {
		generateGitignore()
	}
	return err
}

func removeDatasetSpec() error {
	<-setDataset("", typeNone)
	return nil
}

func deleteDatasetFromServer(datasetID string) *globals.JouleError {
	// Send DELETE request
	fmt.Println("Removing the dataset from Joule.Host...")
	client := api.GlobalClient()
	req := client.R()
	r, err := req.SetPathParams(map[string]string{
		"id": datasetID,
	}).Delete("/dataset/{id}")
	if err != nil {
		return api.NewJouleError(err, "Unable to remove the dataset remotely. Error: %w.\n", err)
	}
	// Use the status code to determine if the request was successful.
	switch r.StatusCode() {
	case http.StatusOK:
		fmt.Println("Done. The dataset was removed from Joule.Host.")
		jww.INFO.Println("Received")
		return nil
	case http.StatusNotFound:
		fmt.Println("Note: the dataset does not exist on the server. No action was taken on Joule.Host.")
		jww.INFO.Println("The dataset does not exist.")
		return nil
	case http.StatusUnauthorized:
		return api.ErrUnauthorized
	case http.StatusInternalServerError:
		return api.ErrInternalServer
	case http.StatusConflict:
		// Returned when verification of upload is still ongoing
		return api.ErrDatasetUndergoingVerification
	case http.StatusBadRequest:
		// The ID is invalid
		fallthrough
	default:
		// FIXME: If the dataset does not belong to the current user, this will also
		// 		  overwrite the current ID. This might be an expected behavior.
		jww.ERROR.Println("Unhandled status code returned:", r.StatusCode())
		return api.UnhandledErrorFromError(&api.UnexpectedReturnCodeError{
			Endpoint: r.Request.URL,
			Method:   r.Request.Method,
			Code:     r.StatusCode(),
		})
	}
}

const gitignorePath = ".gitignore"

var gitignores = []string{
	"*",
	"!" + gitignorePath,
	"!" + datasetConfigFilename,
}

// generateGitignore Generate a Gitignore in the current directory
// containing the content of the `gitignores` variable.
// If a file or directory named .gitignore already exists,
// this method does nothing.
func generateGitignore() {
	if NoGenerateGitignore || fileExists(gitignorePath) {
		jww.TRACE.Println("Skipping `.gitignore` generation.")
		return
	}
	file, err := os.OpenFile(gitignorePath, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		jww.ERROR.Println("Unable to open Gitignore for writing:", err)
		return
	}
	defer file.Close()
	if _, err := file.WriteString(strings.Join(gitignores, "\n") + "\n"); err != nil {
		jww.ERROR.Println("Unable to write Gitignore:", err)
	}
}

// fileExists Returns true if a path corresponds
// to an existing file or directory.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
