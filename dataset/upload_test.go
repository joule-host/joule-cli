package dataset

import (
	"bytes"
	"context"
	"errors"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/google/uuid"
)

const (
	KB = 1_000
	MB = 1_000 * KB
)

var localFiles = []fileUploadInfo{
	{
		Path:        "testdata/upload/Image.jpg",
		ContentType: "image/jpeg",
		Size:        227271,
		MD5Hash:     "UtBlvHkwm09QjYW7i8FxaQ==",
		SHA1Hash:    "wgkYF9SkXX/NiPsQH7/kGuP63Kw=",
	},
	{
		Path:        "testdata/upload/subdir/Movie.mov",
		Size:        40394110,
		ContentType: "application/octet-stream",
		MD5Hash:     "djkg30o+6D1C0UL++U2+ug==",
		SHA1Hash:    "5mq17A9XpMsqOMqEOAxxckdtWkI=",
	},
}

func TestFileIsStillPresent(t *testing.T) {
	t.Parallel()
	// Copy files
	newDirectory, err := ioutil.TempDir("", "joule-cli-test-dir-*")
	if err != nil {
		t.Fatalf("Unable to create temporary directory: %s.", err)
	}
	t.Cleanup(func() {
		_ = os.RemoveAll(newDirectory)
	})
	for _, expected := range localFiles {
		t.Run(expected.Path, func(t *testing.T) {
			expected.Path = copyFile(t, expected.Path, newDirectory)

			if !expected.isAvailableLocally() {
				t.Error("File was reported as not present.")
			}
		})
	}
}

func TestFileIsDifferent(t *testing.T) {
	t.Parallel()
	// Copy files
	newDirectory, err := ioutil.TempDir("", "joule-cli-test-dir-*")
	if err != nil {
		t.Fatalf("Unable to create temporary directory: %s.", err)
	}
	t.Cleanup(func() {
		_ = os.RemoveAll(newDirectory)
	})
	for _, expected := range localFiles {
		t.Run(expected.Path, func(t *testing.T) {
			expected.Path = copyFile(t, expected.Path, newDirectory)
			if err := os.Truncate(expected.Path, 0); err != nil {
				t.Fatalf("Unable to truncate file: %s.", err.Error())
			}

			if expected.isAvailableLocally() {
				t.Error("File was reported as present when it should not.")
			}
		})
	}
}

func TestImageFileRequest(t *testing.T) {
	for _, expected := range localFiles {
		t.Run(expected.Path, func(t *testing.T) {
			errChan := make(chan string)
			l := &sync.Mutex{}
			var errString string
			go func() {
				l.Lock()
				defer l.Unlock()
				for errString = range errChan {
				}
			}()
			f := createFileRequest(expected.Path, errChan)
			close(errChan)
			l.Lock()
			defer l.Unlock()
			if len(errString) > 0 {
				t.Fatalf("Unexpected error returned: %s.\n", errString)
			}
			if f == nil {
				t.Fatalf("File request is nil for file '%s'. This is unexpected.\n", expected.Path)
			}
			t.Run("Path", func(t *testing.T) {
				if f.Path != expected.Path {
					t.Errorf("Path does not match. Expected '%s', got '%s'\n",
						expected.Path, f.Path)
				}
			})
			t.Run("MD5", func(t *testing.T) {
				if f.MD5Hash != expected.MD5Hash {
					t.Errorf("MD5 hash values do not match. Expected '%s', got '%s'\n.",
						expected.MD5Hash, f.MD5Hash)
				}
			})
			t.Run("SHA1", func(t *testing.T) {
				if f.SHA1Hash != expected.SHA1Hash {
					t.Errorf("SHA1 hash values do not match. Expected '%s', got '%s'\n.",
						expected.SHA1Hash, f.SHA1Hash)
				}
			})
			t.Run("ContentType", func(t *testing.T) {
				if f.ContentType != expected.ContentType {
					t.Errorf("Content type values do not match. Expected '%s', got '%s'\n.",
						expected.ContentType, f.ContentType)
				}
			})
			t.Run("Size", func(t *testing.T) {
				if f.Size != expected.Size {
					t.Errorf("Size values do not match. Expected '%d', got '%d'\n.",
						expected.Size, f.Size)
				}
			})
		})
	}
}

func TestBuildChunks(t *testing.T) {

	smallSizes := make([][2]uint, 3_000)
	for i := range smallSizes {
		smallSizes[i] = [2]uint{25 * KB, 0}
	}

	tests := []struct {
		name         string
		fileSizes    [][2]uint
		counterStart uint64
	}{
		{name: "NoInputFile"},
		{
			"NormalFiles1",
			[][2]uint{
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{6 * MB, 8 * MB},
				{1 * MB, 2 * MB},
				{10 * MB, 10 * MB},
				{15 * MB, 0},
			},
			1,
		},
		{
			"NormalFiles2",
			[][2]uint{
				{6 * MB, 8 * MB},
				{10 * MB, 0},
				{1 * MB, 2 * MB},
				{1500 * KB, 2 * MB},
				{10 * MB, 10 * MB},
				{15 * MB, 0},
			},
			1,
		},
		{
			"NormalFilesStartCountAt10",
			[][2]uint{
				{6 * MB, 8 * MB},
				{10 * MB, 0},
				{10 * MB, 10 * MB},
				{1 * MB, 2 * MB},
				{1500 * KB, 2 * MB},
				{10 * MB, 10 * MB},
				{1500 * KB, 2 * MB},
				{15 * MB, 0},
			},
			10,
		},
		{
			"OneBigFile",
			[][2]uint{
				{100 * MB},
			},
			1,
		},
		{
			"ManySmallFiles",
			smallSizes,
			1,
		},
		{
			"FilesHaveSizeOfBatch",
			[][2]uint{
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
			},
			1,
		},
		{
			"BigCompressableFiles",
			[][2]uint{
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
			},
			1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Build FileRequests from paths
			inputPaths := mustGenerateRandomFiles(t, test.fileSizes...)
			filesIn, err := createFileRequestSync(inputPaths)
			if err != nil {
				t.Fatalf("Unable to create file request: %s", err)
			}
			// Build a map of string to FileRequest (used later)
			fileMap := make(map[string]fileUploadInfo, len(filesIn))
			for _, file := range filesIn {
				fileMap[file.Path] = *file
			}
			if len(fileMap) != len(filesIn) {
				t.Fatal("The file map size does not match the array size. Their might be duplicates.")
			}

			// Build chunks from files
			chunks := buildChunksSync(t, test.counterStart, filesIn)

			// Actual tests
			t.Run("ChunksAreOrderedAndAllPresent", func(t *testing.T) {
				for i, chunk := range chunks {
					if chunk.Info.Index != (uint64(i) + test.counterStart) {
						t.Fatalf("Chunks are not ordered properly.")
					}
				}
			})
			t.Run("AllFilesInAreInChunk", func(t *testing.T) {
				files := make(map[string]bool, len(filesIn))
				for _, fileIn := range filesIn {
					files[fileIn.Path] = false
				}
				// no need to check len(files), already checked by len(fileMap)
				for _, chunk := range chunks {
					for _, file := range chunk.Info.Files {
						files[file.Path] = true
					}
				}
				for path, ok := range files {
					if !ok {
						t.Errorf("File '%s' in input was not in any chunk.", path)
					}
				}
			})
			t.Run("AllFilesInChunkMatchFileIn", func(t *testing.T) {
				for _, chunk := range chunks {
					for _, file := range chunk.Info.Files {
						expected, found := fileMap[file.Path]
						if !found {
							t.Fatal("File in chunk does not exist in File Map.")
						}
						if !cmp.Equal(*file, expected,
							cmpopts.IgnoreFields(fileUploadInfo{}, "ByteRange")) {
							// Check for all attributes except ByteRange
							t.Fatal("A file in chunk does not match its original.")
						}
					}
				}
			})
			t.Run("OnlyOneSplitFilePerChunk", func(t *testing.T) {
				var previousSplitFile *fileUploadInfo
				var nextSplitFile *fileUploadInfo
				for _, chunk := range chunks { // note: chunks must be sorted by index
					var filesNotStartingAtStart uint
					var filesNotEndingAtEnd uint
					for _, file := range chunk.Info.Files {
						if file.ByteRange.Start != 0 {
							filesNotStartingAtStart++
							// This must be the split file from the previous chunk
							if previousSplitFile == nil {
								t.Fatal("File is split when the previous chunk had no split file.")
							}
							if !cmp.Equal(*file, *previousSplitFile,
								cmpopts.IgnoreFields(fileUploadInfo{}, "ByteRange")) {
								t.Fatal("Split file in chunk does not match previous one")
							}
							// The byte range must match
							if file.ByteRange.Start != previousSplitFile.ByteRange.End {
								t.Fatal("Split file in chunk is not contiguous with previous one")
							}
						}
						if file.ByteRange.End != file.Size {
							filesNotEndingAtEnd++
							nextSplitFile = file
						}
					}
					if filesNotStartingAtStart > 1 {
						t.Errorf("The chunk should only contain one file split with previous chunk.")
					}
					if filesNotEndingAtEnd > 1 {
						t.Errorf("The chunk should only contain one file split with next chunk.")
					}
					previousSplitFile = nextSplitFile
				}
			})
			t.Run("OnlyTheLastChunkBelowTheChunkSizeLimit", func(t *testing.T) {
				for i, chunk := range chunks {
					if int(chunk.Info.Size) < minChunkSize && (i+1) != len(chunks) {
						t.Fatal("Only the last chunk can be under the `minChunkSize`.")
					}
				}
			})
			t.Run("ChunksAreNotTooMuchOverTheSizeTarget", func(t *testing.T) {
				for _, chunk := range chunks {
					if (chunk.Info.Size) > (batchSizeBytes + 1*MB) {
						t.Fatalf("Chunk size (%d bytes) is too much over `batchSizeBytes`.", chunk.Info.Size)
					}
				}
			})

			// Generate byte ranges
			bytesRangesMap := make(map[string][]byteRange)
			for _, chunk := range chunks {
				for _, file := range chunk.Info.Files {
					bytesRangesMap[file.Path] = append(bytesRangesMap[file.Path], file.ByteRange)
				}
			}
			// Sort arrays
			for path := range bytesRangesMap {
				byteRanges := bytesRangesMap[path]
				sort.Slice(byteRanges, func(i, j int) bool {
					return byteRanges[i].Start < byteRanges[j].Start
				})
				bytesRangesMap[path] = byteRanges
			}
			t.Run("ByteRanges", func(t *testing.T) {
				for p, byteRanges := range bytesRangesMap {
					previous := uint(0)
					for _, fileByteRange := range byteRanges {
						if previous != fileByteRange.Start {
							t.Fatal("Byte range is not contiguous.")
						}
						previous = fileByteRange.End
					}
					if previous != fileMap[p].Size {
						t.Fatal("Byte Ranges do not include the whole file.")
					}
				}
			})
			// TODO:
			//  - Tests on resumed uploads (where SplitFile != nil for instance)
			//  - Rebuild files (might be done in another test instead)
		})
	}
}

func buildChunksSync(t *testing.T, counterStart uint64, filesIn []*fileUploadInfo) []*chunkWithBuffer {
	chunks := make([]*chunkWithBuffer, 0, len(filesIn))
	chunksC := make(chan *chunkWithBuffer)
	errChan := make(chan string)
	progressChan := make(chan int64)

	go func() {
		defer close(progressChan)
		defer close(errChan)
		defer close(chunksC)

		counter := &counterType{
			pointer: counterStart,
		}
		buildChunks(context.Background(), filesIn, chunksC, nil, errChan, counter, progressChan)
	}()

	for chunksC != nil {
		select {
		case chunk, open := <-chunksC:
			if !open {
				chunksC = nil
				break
			}
			chunks = append(chunks, chunk)
		case _, stillOpen := <-progressChan:
			if !stillOpen {
				progressChan = nil
				break
			}
		case errStr, stillOpen := <-errChan:
			if !stillOpen {
				errChan = nil
				break
			}
			t.Errorf("Received an error: %s.", errStr)
		}
	}
	return chunks
}

func TestBuildChunkFromInfo(t *testing.T) {

	tests := []struct {
		name         string
		fileSizes    [][2]uint
		counterStart uint64
	}{
		{name: "NoInputFile"},
		{"NormalFiles1",
			[][2]uint{
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{1 * MB, 2 * MB},
				{6 * MB, 8 * MB},
				{1 * MB, 2 * MB},
				{10 * MB, 10 * MB},
				{15 * MB, 0},
			},
			1,
		},
		{
			"NormalFiles2",
			[][2]uint{
				{6 * MB, 8 * MB},
				{10 * MB, 0},
				{1 * MB, 2 * MB},
				{1500 * KB, 2 * MB},
				{10 * MB, 10 * MB},
				{15 * MB, 0},
			},
			1,
		},
		{
			"NormalFilesStartCountAt10",
			[][2]uint{
				{6 * MB, 8 * MB},
				{10 * MB, 0},
				{10 * MB, 10 * MB},
				{1 * MB, 2 * MB},
				{1500 * KB, 2 * MB},
				{10 * MB, 10 * MB},
				{1500 * KB, 2 * MB},
				{15 * MB, 0},
			},
			10,
		},
		{
			"OneBigFile",
			[][2]uint{
				{100 * MB},
			},
			1,
		},
		{
			"FilesHaveSizeOfBatch",
			[][2]uint{
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
				{batchSizeBytes, 0},
			},
			1,
		},
		{
			"BigCompressableFiles",
			[][2]uint{
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
				{500 * KB, 2 * batchSizeBytes},
			},
			1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Build FileRequests from paths
			inputPaths := mustGenerateRandomFiles(t, test.fileSizes...)
			filesIn, err := createFileRequestSync(inputPaths)
			if err != nil {
				t.Fatalf("Unable to create file request: %s", err)
			}
			// Build a map of string to FileRequest (used later)
			fileMap := make(map[string]fileUploadInfo, len(filesIn))
			for _, file := range filesIn {
				fileMap[file.Path] = *file
			}
			if len(fileMap) != len(filesIn) {
				t.Fatal("The file map size does not match the array size. Their might be duplicates.")
			}

			// Build chunks from files
			pairs := make([][2]*chunkWithBuffer, 0, len(filesIn))
			{
				ctx, cancel := context.WithCancel(context.Background())
				pairsC := make(chan [2]*chunkWithBuffer)
				errChan := make(chan string)
				progressChan := make(chan int64)

				{
					chunks := make(chan *chunkWithBuffer)
					go func() {
						defer close(progressChan)
						defer close(errChan)
						defer close(chunks)

						counter := &counterType{
							pointer: test.counterStart,
						}
						buildChunks(ctx, filesIn, chunks, nil, errChan, counter, progressChan)
					}()

					go func() {
						defer close(pairsC)
						for chunk := range chunks {
							out, err := chunk.Info.buildChunkFromChunkInfo()
							if err != nil {
								t.Errorf("Unable to build chunk from info: %s", err.Error())
								cancel()
								return
							}
							select {
							case pairsC <- [2]*chunkWithBuffer{chunk, out}:
							case <-ctx.Done():
								t.Log("Context was canceled before end.")
								return
							}
						}
					}()
				}

				for pairsC != nil {
					select {
					case pair, open := <-pairsC:
						if !open {
							pairsC = nil
							break
						}
						pairs = append(pairs, pair)
					case _, stillOpen := <-progressChan:
						if !stillOpen {
							progressChan = nil
							break
						}
					case errStr, stillOpen := <-errChan:
						if !stillOpen {
							errChan = nil
							break
						}
						cancel()
						t.Errorf("Received an error: %s.", errStr)
					}
				}
			}

			// Actual tests
			for _, pair := range pairs {
				expected, actual := pair[0], pair[1]
				if !bytes.Equal(expected.bytes, actual.bytes) {
					t.Errorf("Expected the two byte arrays to be the same.")
				}
				if !cmp.Equal(actual.Info, expected.Info) {
					// Check for all attributes except ByteRange
					t.Fatal("Chunk built does not have the expected info.")
				}
			}
		})
	}
}

func createFileRequestSync(paths []string) ([]*fileUploadInfo, error) {
	errChan := make(chan string)
	l := &sync.Mutex{}
	var err error
	go func() {
		l.Lock()
		defer l.Unlock()
		for errString := range errChan {
			err = errors.New(errString)
		}
	}()
	out := make([]*fileUploadInfo, len(paths))
	for i, path := range paths {
		out[i] = createFileRequest(path, errChan)
	}
	close(errChan)
	l.Lock()
	defer l.Unlock()
	return out, err
}

// mustGenerateRandomFiles Generate random files following the size specification from `sizes`.
// The files will be removed from disk on clean-up of `t`, the current test.
func mustGenerateRandomFiles(t *testing.T, sizes ...[2]uint) []string {
	randomFiles, err := generateRandomFiles(sizes)
	if err != nil {
		t.Fatalf("Unable to generate random files: %s.", err.Error())
	}
	t.Cleanup(func() {
		for _, path := range randomFiles {
			_ = os.Remove(path)
		}
	})
	return randomFiles
}

func generateRandomFiles(sizes [][2]uint) ([]string, error) {
	paths := make([]string, len(sizes))
	for i, size := range sizes {
		var err error
		paths[i], err = createRandomFile(size[0], size[1])
		if err != nil {
			return paths, err
		}
	}
	return paths, nil
}

// createRandomFile Create a random file that contains `compressedSize` random bytes,
// and `totalSize-compressedSize` '0'-bytes (that will be compressed).
//
// Note: This implementation makes use of buffers. It would have to be
// re-implemented with io.Pipes to avoid using too much RAM.
func createRandomFile(compressedSize uint, totalSize uint) (string, error) {
	f, err := ioutil.TempFile("", "joule-cli-test-*.random")
	if err != nil {
		return "", err
	}
	defer f.Close()
	// Write the random bytes
	{ // Use a scope so that `buff` can be garbage collected quickly
		buff := make([]byte, compressedSize)
		rand.Read(buff)
		_, err = f.Write(buff)
		if err != nil {
			return "", err
		}
	}
	if totalSize > compressedSize {
		// Append "0"-bytes at the end of the buffer.
		// Note that this implementation might be faster if we use f.Seek(totalSize) instead.
		_, err = f.Write(make([]byte, totalSize-compressedSize))
	}
	return f.Name(), err
}

func benchmarkCreateFileRequest(b *testing.B, nb uint, files ...string) {
	length := uint(len(files))
	// run on nb files b.N times
	for n := 0; n < b.N; n++ {
		// Run on nb files
		for i := uint(0); i < nb; i++ {
			errChan := make(chan string)
			l := &sync.Mutex{}
			var errString string
			go func() {
				l.Lock()
				defer l.Unlock()
				for errString = range errChan {
				}
			}()
			f := createFileRequest(files[i%length], errChan)
			close(errChan)
			l.Lock()
			if len(errString) > 0 {
				b.Fatalf("Unexpected error returned: %s.\n", errString)
			}
			l.Unlock()
			if f == nil {
				b.Fatalf("Call on file '%s' returned nil.\n", files[i%length])
			}
		}
	}
}

func BenchmarkCreate50FileRequests(b *testing.B) {
	benchmarkCreateFileRequest(b, 50, "testdata/upload/Image.jpg", "testdata/upload/subdir/Movie.mov")
}

func BenchmarkSortFileUploadInfo(b *testing.B) {
	toCopy := generateSliceOfFileUploads(10_000, 100, 3_000*MB, 20*MB)
	for i := 0; i < b.N; i++ {
		newFilesToUpload := make([]*fileUploadInfo, 0)
		for _, f := range toCopy {
			newFilesToUpload = append(newFilesToUpload, f)
		}
		sort.Sort(BySizePathByteRange(newFilesToUpload))
	}
}

// Generates a slice of size, size filled with random numbers
func generateSliceOfFileUploads(nbFiles, minSize, maxSize, chunkSize uint) []*fileUploadInfo {
	slice := make([]*fileUploadInfo, 0, 4*nbFiles)
	rand.Seed(0)
	for i := uint(0); i < nbFiles; i++ {
		size := uint(rand.Intn(int(maxSize-minSize))) + minSize
		path := uuid.New().String()
		for start := uint(0); start < size; start += chunkSize {
			end := start + chunkSize
			if end > size {
				end = size
			}
			slice = append(slice, &fileUploadInfo{
				Path:      path,
				Size:      size,
				ByteRange: byteRange{start, end},
			})
		}
	}
	return slice
}

func copyFile(t *testing.T, src, dstDirectory string) string {
	newPath := filepath.Join(dstDirectory, src)
	if err := os.MkdirAll(filepath.Dir(newPath), 0700); err != nil {
		t.Fatalf("Unable to create directory to copy file: %s.", err.Error())
	}

	sourceFileStat, err := os.Stat(src)
	if err != nil {
		t.Fatalf("Unable to stat file: %s.", err.Error())
	}

	if !sourceFileStat.Mode().IsRegular() {
		t.Fatalf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		t.Fatalf("Unable to Open src file: %s.", err.Error())
	}
	defer source.Close()

	destination, err := os.Create(newPath)
	if err != nil {
		t.Fatalf("Unable to create dst file: %s.", err.Error())
	}
	defer destination.Close()

	_, err = io.Copy(destination, source)
	if err != nil {
		t.Fatalf("Unable to copy file: %s.", err.Error())
	}

	t.Cleanup(func() {
		_ = os.Remove(newPath)
	})
	return newPath
}
