package dataset

import (
	"archive/zip"
	"bytes"
	"compress/flate"
	"context"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strconv"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/go-resty/resty/v2"
	"github.com/karrick/godirwalk"
	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/vbauerster/mpb/v4"

	"gitlab.com/joule-host/joule-cli/api"
	"gitlab.com/joule-host/joule-cli/globals"
)

// ForceUpload When set to true, forces a re-upload of all the files // TODO
var ForceUpload bool

const (
	batchSizeBytes uint = 20 * 1000 * 1000 // Size of a chunk in bytes for the sign URL requests
)

var (
	// The minimum size a chunk must imperatively have to be valid (except the last chunk).
	minChunkSize = 5 * 1024 * 1024

	// The number of parallel uploaders
	nbUploaders uint = 4

	counter = counterType{
		pointer: 1, // Start counting at 1
	}
)

type counterType struct {
	sync.Mutex
	pointer     uint64
	alreadyUsed []bool
}

func (c *counterType) growToIndex(idx uint64) {
	l := uint64(len(c.alreadyUsed))
	if idx >= l {
		// Grow to current capacity, or to at least idx + 1
		minimumSize := idx + 1
		newSize := uint64(cap(c.alreadyUsed))
		if newSize < minimumSize {
			newSize = minimumSize
		}
		c.alreadyUsed = append(c.alreadyUsed, make([]bool, newSize-l)...)
	}
}

func (c *counterType) addUsedIndex(v uint64) {
	c.growToIndex(v)
	c.alreadyUsed[v] = true
}

func (c *counterType) nextIndex() uint64 {
	c.Lock()
	defer c.Unlock()
	c.growToIndex(c.pointer)
	for c.pointer == 0 || c.alreadyUsed[c.pointer] {
		c.pointer++
		c.growToIndex(c.pointer)
	}
	// c.pointer points to a free index
	c.addUsedIndex(c.pointer) // We mark it as used
	return c.pointer
}

type byteRange struct {
	Start uint `json:"start"`
	End   uint `json:"end"`
}

type fileUploadInfo struct {
	Path        string    `json:"path"`
	ContentType string    `json:"content_type"`
	Size        uint      `json:"size"`
	ByteRange   byteRange `json:"byte_range"`
	MD5Hash     string    `json:"md5_hash"`
	SHA1Hash    string    `json:"sha1_hash"`
}

type uploadedFileT struct {
	FilePath        string `json:"file_path"`
	FileSize        uint   `json:"file_size"`
	FileContentType string `json:"file_content_type"`
	FileSHA         string `json:"sha1_hash"`
	FileMD5         string `json:"md5_hash"`
	ChunkIndex      uint64 `json:"chunk_index"`
	AlreadyUploaded bool   `json:"already_uploaded"`
	FileChunkStart  uint   `json:"file_chunk_start"`
	FileChunkEnd    uint   `json:"file_chunk_end"`
}

func (info fileUploadInfo) chunkSize() uint {
	return info.ByteRange.End - info.ByteRange.Start
}

type chunkInfo struct {
	Index    uint64            `json:"index"`
	Files    []*fileUploadInfo `json:"files"`
	Size     uint              `json:"size"`
	SHA1Hash string            `json:"sha_hash"`
}

type chunkWithBuffer struct {
	Info  chunkInfo
	bytes []byte
}

type startUploadResponse struct {
	AlreadyStarted         bool   `json:"already_started"`
	UploadID               string `json:"upload_id"`
	DatasetName            string `json:"dataset_name"`
	DatasetProtocolVersion string `json:"dataset_protocol_version"`
	MinimumChunkSize       int    `json:"minimum_chunk_size"`
}

type uploadAuthRequest struct {
	UploadURL         string            `json:"upload_url"`
	Headers           map[string]string `json:"headers"`
	AdditionalHeaders struct {
		PartIndex string `json:"part_index"`
		PartSHA1  string `json:"part_sha1"`
	} `json:"additional_headers"`
}

// UploadDataset The Cobra command to upload the dataset
// represented by the local configuration.
func UploadDataset(_ *cobra.Command, _ []string) error {
	jww.TRACE.Println("Starting a new upload.")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Make sure the dataset exists on the server
	if !datasetExists(nil) {
		return globals.ErrDatasetDoesNotExist
	}

	newUploadParams, err := requestStartNewUpload()
	if err != nil {
		return err
	}
	if newUploadParams.MinimumChunkSize > minChunkSize {
		minChunkSize = newUploadParams.MinimumChunkSize
	}
	sentry.ConfigureScope(func(s *sentry.Scope) {
		s.SetExtra("new_upload_params", fmt.Sprintf("%+v", *newUploadParams))
		s.SetExtra("upload_is_resume", newUploadParams.AlreadyStarted)
	})
	if newUploadParams.DatasetProtocolVersion != globals.JouleProtocolVersion {
		sentry.ConfigureScope(func(s *sentry.Scope) {
			s.SetExtra("client_protocol_version", globals.JouleProtocolVersion)
			s.SetExtra("server_protocol_version", newUploadParams.DatasetProtocolVersion)
			s.SetExtra("dataset_id", ID())
		})
		sentry.CaptureException(globals.ErrUnknownProtocolVersion)
		return globals.ErrUnknownProtocolVersion
	}
	if newUploadParams.AlreadyStarted {
		jww.INFO.Println("Resuming dataset upload...")
		fmt.Printf("Resuming upload of '%s'...\n", newUploadParams.DatasetName)
	} else {
		jww.INFO.Println("Starting new dataset upload...")
		fmt.Printf("Starting upload of '%s'...\n", newUploadParams.DatasetName)
	}

	// Create the Progress bar
	var progressBarTotal int64
	p, bar := createProgressBar(progressBarTotal)

	// Figure out which chunks should be uploaded/re-uploaded/skipped
	chunksToReupload := make(map[uint64]chunkInfo)
	var splitFile *fileUploadInfo
	skippedFilePathsSet := make(map[string]bool) // Set of paths that are skipped when looking for files if their flag is true
	{
		uploadedFilesChan, errChan := requestUploadedFilesAsync(ctx, ID(), newUploadParams.UploadID)
		counter.Lock()
		for uploadedFiles := range uploadedFilesChan {
			// Find split file (will be overrode on each loop)
			var err error
			splitFile, err = findSplitFile(uploadedFiles)
			if err != nil {
				return err
			}
			// Figure out which chunks should be uploaded/re-uploaded/skipped
			for _, uploadedFile := range uploadedFiles {
				counter.addUsedIndex(uploadedFile.ChunkIndex)
				fileInfo := uploadedFile.asFileUploadInfo()
				// Add the size of the files that will be reuploaded to the total of the progress bar
				// This will be adjusted with the compressed size later
				progressBarTotal += int64(fileInfo.chunkSize())
				bar.SetTotal(progressBarTotal+1, false)
				// Mark file path as uploaded
				skippedFilePathsSet[uploadedFile.FilePath] = true
				// Add the file to the chunks of files to reupload.
				if chunk, ok := chunksToReupload[uploadedFile.ChunkIndex]; ok {
					// Chunk already exists
					if uploadedFile.AlreadyUploaded {
						// Could be checked earlier, but just to make sure all assumptions are respected
						err := errors.New("the server broke some assumptions about the previous upload: \n" +
							"files from a common chunk don't have the same 'already_uploaded' state")
						sentry.CaptureException(err)
						return err
					}
					chunk.Files = append(chunk.Files, fileInfo)
					chunksToReupload[uploadedFile.ChunkIndex] = chunk
				} else if !uploadedFile.AlreadyUploaded {
					// Create a new chunk to upload as this chunk was not done uploading
					chunksToReupload[uploadedFile.ChunkIndex] = chunkInfo{
						Index: uploadedFile.ChunkIndex,
						Files: []*fileUploadInfo{fileInfo},
						// Size and SHA1Hash cannot be known
					}
				} else {
					// The chunk for the file does not exist but it is already uploaded
					// so we will not need to upload one anyway.
					// Add it to the progress bar.
					bar.IncrBy(int(fileInfo.chunkSize()))
				}
			}
		}
		counter.Unlock()

		if err := <-errChan; err != nil {
			return err
		}
	}

	// Ensure all files to re-upload are available locally.
	if len(chunksToReupload) > 0 {
		fileSet := make(map[string]*fileUploadInfo)
		for _, c := range chunksToReupload {
			for _, f := range c.Files {
				fileSet[f.Path] = f
			}
		}
		for _, fileInfo := range fileSet {
			if !fileInfo.isAvailableLocally() {
				return globals.ErrFilesDoNotExistLocally
			}
		}
	}

	// Create a Go routine that will upgrade the progress bar total
	pbarTotalUpdates := make(chan int64)
	defer close(pbarTotalUpdates)
	go func() {
		// This function will wait for progress bar updates and set the updates on the bar
		jww.TRACE.Printf("Listening to Progress Bar updates.\n\n")
		for u := range pbarTotalUpdates {
			progressBarTotal += u
			if progressBarTotal < 0 {
				jww.ERROR.Println("Unexpected: progress bar total is negative.")
				sentry.CaptureMessage("progress bar total is negative")
				progressBarTotal = 0
			}
			bar.SetTotal(progressBarTotal, false)
		}
		jww.TRACE.Printf("Done listening to Progress Bar updates.\n\n")
	}()

	// Walk through files in the directory and add them to `fileUploadInfo`
	// (if they were not already in `skippedFilePathsSet`)
	newFilesToUpload := make([]*fileUploadInfo, 0)
	errChan := make(chan string)
	errs := make([]string, 0)
	errWg := &sync.WaitGroup{}
	errWg.Add(1)
	go func() {
		defer errWg.Done()
		for toShow := range errChan {
			errs = append(errs, toShow)
		}
	}()
	{
		chanFiles := make(chan *fileUploadInfo)
		chanPaths := make(chan string)

		// Create a few workers that receive paths from `chanPaths`, take the file pointed by the path,
		// hash them, compress them, and push them to `chanFiles`
		wg := &sync.WaitGroup{}
		for i := uint(0); i < nbUploaders; i++ {
			wg.Add(1)
			go fileWorker(chanPaths, chanFiles, wg, errChan)
		}
		go func() {
			// Fan out paths from the current directory to the file upload builder.
			err := filesInCurrentDirectory(chanPaths, skippedFilePathsSet)
			if err != nil {
				jww.ERROR.Printf("Got an error listing files: %s\n\n", err.Error())
				errChan <- err.Error()
			}
			// Waits for all the file workers to finish,
			// and then closes the chanFiles chanel.
			wg.Wait()
			close(chanFiles)
		}()

		// Aggregate result
		for file := range chanFiles {
			newFilesToUpload = append(newFilesToUpload, file)
			// Add the size of the file to the Progress Bar total, although
			// we'll adjust it later to account for the difference in size with the (compressed) chunk
			pbarTotalUpdates <- int64(file.Size)
		}
		jww.TRACE.Printf("Done going through files\n\n")
	}

	// Start chunk builders that take chunk info and create a compressed zip archive of all included files.
	chanChunks := make(chan *chunkWithBuffer)
	go func() {
		defer jww.TRACE.Println("Chunks channel closed.")
		defer close(chanChunks)
		defer jww.DEBUG.Println("Closing chunks channel...")
		jww.TRACE.Printf("Creating upload chunks...\n\n")

		// Start re-uploading chunks that were not complete in previous uploads
		for _, c := range chunksToReupload {
			cBuffer, err := c.buildChunkFromChunkInfo()
			if err != nil {
				errChan <- err.Error()
				continue
			}
			// Adjust progress bar size
			progressBarAdjust := int64(cBuffer.Info.Size)
			for _, file := range cBuffer.Info.Files {
				progressBarAdjust -= int64(file.chunkSize())
			}
			pbarTotalUpdates <- progressBarAdjust
			// Send chunk
			select {
			case chanChunks <- cBuffer:
				jww.TRACE.Println("Sent re-upload chunk")
			case <-ctx.Done():
				jww.TRACE.Println("context was canceled")
				return
			}
		}

		// Sort the array of new files to upload
		sort.Sort(BySizePathByteRange(newFilesToUpload))
		// Start uploading all chunks that are a new
		buildChunks(ctx, newFilesToUpload, chanChunks, splitFile, errChan, &counter, pbarTotalUpdates)
	}()

	chanUploads := make(chan *chunkWithBuffer)
	// TODO: only use one uploader if the folder is too small
	// Start upload workers that take upload requests in input,
	// and handle the uploading of files to the cloud.
	gUploadWrkrs := &sync.WaitGroup{}
	for i := uint(0); i < nbUploaders; i++ {
		gUploadWrkrs.Add(1)
		go uploadWorker(ctx, cancel, newUploadParams.UploadID, bar, errChan, chanUploads, gUploadWrkrs)
	}

	// Start Upload Signing Requester
	go signingWorker(ctx, cancel, chanChunks, newUploadParams.UploadID, chanUploads, errChan)

	go func() {
		// Waits for termination and closes the error channel to signal
		// that no more errors can happen.
		gUploadWrkrs.Wait()
		jww.DEBUG.Printf("Closing ErrorChan.\n\n")
		close(errChan) // FIXME: If any goroutine stops, this could never happen.
		jww.TRACE.Printf("Closed ErrorChan.\n\n")
	}()
	errWg.Wait() // Wait for all errors
	bar.SetTotal(bar.Current(), true)
	p.Wait()
	if len(errs) > 0 {
		fmt.Println("Some errors happened during upload:")
	} else {
		fmt.Println("Awaiting server confirmation...")
		err = requestFinishUpload(newUploadParams.UploadID)
		if err != nil {
			return err
		}
		fmt.Println("Upload complete.")
	}
	for _, toShow := range errs {
		fmt.Print(toShow)
	}
	return nil
}

// findSplitFile Figure which file was being split in the last upload
// (there should be at most one in version 1.0 of the protocol)
// Return `nil` if no file was split.
// The files are supposedly sorted by the server in ascending order of chunk, name, and byte_start.
// The split file is in the last chunk.
// WARNING: No assertion checks are provided by this function.
//          An invalid upload that messed up the split file will not be detected by this method.
func findSplitFile(previousUploads []uploadedFileT) (*fileUploadInfo, error) {
	var splitFile *fileUploadInfo = nil
	if len(previousUploads) > 0 {
		lastChunkIdx := previousUploads[len(previousUploads)-1].ChunkIndex
		for i := len(previousUploads) - 1; i >= 0 && splitFile == nil; i-- {
			if previousUploads[i].ChunkIndex < lastChunkIdx {
				// We are not in the
				break
			}
			if previousUploads[i].FileChunkEnd != previousUploads[i].FileSize {
				// Only one file is supposed to be incomplete: this is the split file.
				splitFile = previousUploads[i].asFileUploadInfo()
			}
		}
	}
	if splitFile != nil {
		splitFile.ByteRange.Start = splitFile.ByteRange.End
		splitFile.ByteRange.End = splitFile.Size
		if !splitFile.isAvailableLocally() {
			return nil, globals.ErrFilesDoNotExistLocally
		}
	}
	return splitFile, nil
}

// BySizePathByteRange implements a sort on []*fileUploadInfo
// by order Size, then Path, and then ByteRange Start.
type BySizePathByteRange []*fileUploadInfo

func (a BySizePathByteRange) Len() int      { return len(a) }
func (a BySizePathByteRange) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a BySizePathByteRange) Less(i, j int) bool {
	left, right := a[i], a[j]
	return left.Size < right.Size ||
		(left.Size == right.Size &&
			(left.Path < right.Path ||
				(left.Path == right.Path && left.ByteRange.Start < right.ByteRange.End)))
}

func (uploadedFile *uploadedFileT) asFileUploadInfo() *fileUploadInfo {
	return &fileUploadInfo{
		Path:        uploadedFile.FilePath,
		ContentType: uploadedFile.FileContentType,
		Size:        uploadedFile.FileSize,
		ByteRange: struct {
			Start uint `json:"start"`
			End   uint `json:"end"`
		}{Start: uploadedFile.FileChunkStart, End: uploadedFile.FileChunkEnd},
		MD5Hash:  uploadedFile.FileMD5,
		SHA1Hash: uploadedFile.FileSHA,
	}
}

func requestStartNewUpload() (*startUploadResponse, error) {
	client, err := api.NewClient()
	if err != nil {
		return nil, err
	}
	client.SetPathParams(map[string]string{"dataset_id": ID()})
	resp, err := client.R().
		SetResult(&startUploadResponse{}).
		SetError(&api.JouleErrorResponse{}).
		Post("/dataset/{dataset_id}/upload")
	if err != nil {
		return nil, err
	}
	switch resp.StatusCode() {
	case http.StatusOK:
		uploadResponse, ok := resp.Result().(*startUploadResponse)
		if !ok || len(uploadResponse.UploadID) == 0 {
			return nil, errors.New("unexpected server response when requesting upload URL")
		}
		return uploadResponse, nil
	case http.StatusUnauthorized:
		return nil, api.ErrUnauthorized
	case http.StatusAlreadyReported:
		return nil, globals.ErrDatasetUploadComplete
	case http.StatusInternalServerError:
		return nil, errors.New("the server is having issues starting our upload, this is " +
			"likely temporary and will be fixed soon, please try again later")
	case http.StatusNotFound:
		return nil, api.ErrDatasetDoesNotExistAnymore
	default:
		return nil, api.UnhandledErrorFromError(
			fmt.Errorf("unexpected server response code (%d) when starting new upload",
				resp.StatusCode()))
	}
}

func requestUploadedFilesAsync(ctx context.Context, datasetID, uploadID string) (<-chan []uploadedFileT, <-chan error) {
	out := make(chan []uploadedFileT, 3) // Buffered to do some file requests ahead if possible
	errCh := make(chan error, 1)         // Size of 1 to avoid blocking if the context was canceled.
	go func() {
		defer close(errCh)
		defer close(out)
		var startFrom uint
		for {
			uploadedFiles, next, err := requestUploadedFiles(datasetID, uploadID, startFrom)
			if err != nil {
				errCh <- err
				return
			}
			select {
			case <-ctx.Done():
				jww.INFO.Printf("Context was canceled before `requestUploadedFilesAsync` could send an output.\n\n")
				return
			case out <- uploadedFiles:
			}
			if next > startFrom {
				startFrom = next
			} else {
				return
			}
		}
	}()
	return out, errCh
}

func requestUploadedFiles(datasetID, uploadID string, startFrom uint) ([]uploadedFileT, uint, error) {
	type responseT struct {
		PreviousUploadsSorted []uploadedFileT `json:"previous_uploads_sorted"`
		Next                  struct {
			StartFrom uint `json:"start_from"`
		} `json:"next"`
	}
	req := api.GlobalClient().R()
	if startFrom > 0 {
		req.SetQueryParam("start_from", strconv.Itoa(int(startFrom)))
	}

	resp, err := req.
		SetError(&api.JouleErrorResponse{}).
		SetResult(&responseT{}).
		SetPathParams(map[string]string{
			"dataset_id": datasetID,
			"upload_id":  uploadID,
		}).
		Get("/dataset/{dataset_id}/upload/{upload_id}/files")
	if err != nil {
		return nil, 0, errors.New("unable to retrieve dataset files: " + err.Error())
	}

	switch resp.StatusCode() {
	case http.StatusOK:
		datasetFiles, ok := resp.Result().(*responseT)
		if !ok || datasetFiles == nil {
			err = errors.New("unable to understand server's response")
			return nil, 0, err
		}
		return datasetFiles.PreviousUploadsSorted, datasetFiles.Next.StartFrom, err
	case http.StatusNotFound:
		jww.INFO.Println("The dataset does not exist.")
		return nil, 0, api.ErrDatasetDoesNotExistAnymore
	case http.StatusUnauthorized:
		return nil, 0, api.ErrUnauthorized
	case http.StatusInternalServerError:
		return nil, 0, api.ErrInternalServer
	case http.StatusConflict:
		// Returned when a newer upload is undergoing
		return nil, 0, api.ErrDatasetHasNewerUpload
	case http.StatusBadRequest, http.StatusPreconditionFailed:
		// The request is malformed
		sentry.ConfigureScope(func(s *sentry.Scope) {
			s.SetExtras(map[string]interface{}{
				"dataset_id":  datasetID,
				"upload_id":   uploadID,
				"return_code": resp.StatusCode(),
			})
		})
		sentry.CaptureMessage("Unexpected API behavior.")
		fallthrough
	default:
		jww.ERROR.Println("Unhandled status code returned:", resp.StatusCode())
		return nil, 0, api.UnhandledErrorFromError(&api.UnexpectedReturnCodeError{
			Endpoint: resp.Request.URL,
			Method:   resp.Request.Method,
			Code:     resp.StatusCode(),
		})
	}
}

func requestFinishUpload(uploadID string) error {
	jww.TRACE.Printf("`requestFinishUpload` called.\n\n")
	defer jww.TRACE.Printf("`requestFinishUpload` returned.\n\n")
	client, err := api.NewClient()
	if err != nil {
		return err
	}
	client.SetPathParams(map[string]string{"dataset_id": ID(), "upload_id": uploadID})
	jww.TRACE.Printf("Posting FinishUpload request...\n\n")
	resp, err := client.R().
		Post("/dataset/{dataset_id}/upload/{upload_id}/finish")
	if err != nil {
		return err
	}
	jww.TRACE.Printf("FinishUpload request returned.")
	switch resp.StatusCode() {
	case http.StatusOK, http.StatusNoContent:
		return nil
	case http.StatusInternalServerError:
		return errors.New("the server is having issues finishing our upload, this is " +
			"likely temporary and will be fixed soon, please try again later")
	case http.StatusExpectationFailed, http.StatusPreconditionFailed:
		return errors.New("the server reported that the upload was not started, this is a bug")
	case http.StatusAlreadyReported:
		return globals.ErrDatasetUploadComplete
	case http.StatusConflict:
		return errors.New("the server reported that a new upload from another is now running, " +
			"please make sure you are uploading from one client instance of Joule")
	case http.StatusNotFound:
		return errors.New("the server reported that the dataset was removed while finishing the upload")
	default:
		return fmt.Errorf("unexpected server response code (%d) when requesting upload URL",
			resp.StatusCode())
	}
}

// buildChunks
func buildChunks(ctx context.Context, filesIn []*fileUploadInfo, chunksOutChan chan<- *chunkWithBuffer, firstFile *fileUploadInfo, errorsChan chan string, chunkCounter *counterType, progressBarTotal chan<- int64) {
	batchSizeBytesInt64 := int64(batchSizeBytes) // Used for comparisons with integers
	var file struct {
		f    *os.File
		info *fileUploadInfo
	}
	if firstFile != nil {
		var err error
		file.info = firstFile
		file.f, err = os.Open(file.info.Path)
		if err != nil {
			errorsChan <- fmt.Sprintf("Unable to read file at path '%s'. Error: %s. "+
				"Skipping...\n", file.info.Path, err)
			file.f = nil
		}
		if _, err := file.f.Seek(int64(firstFile.ByteRange.Start), io.SeekStart); err != nil {
			errorsChan <- fmt.Sprintf("Unable to seek in file at path '%s'. Error: %s. "+
				"Skipping...\n", file.info.Path, err)
			_ = file.f.Close()
			file.f = nil
		}
	}
	i := 0
	for i < len(filesIn) || file.f != nil {
		// New chunk
		realTotalSizeFiles := int64(0)
		batch := make([]*fileUploadInfo, 0)
		buf := new(bytes.Buffer)
		buf.Grow(minChunkSize) // Allocate the minimum chunk size
		// Create Zip and SHA1 writers.
		hSHA := sha1.New()
		zw := zip.NewWriter(io.MultiWriter(buf, hSHA))
		zw.RegisterCompressor(zip.Deflate, func(out io.Writer) (io.WriteCloser, error) {
			return flate.NewWriter(out, flate.BestCompression)
		})
		// Loop over files
		for remainingSpaceInChunk := batchSizeBytesInt64; remainingSpaceInChunk > 0; remainingSpaceInChunk = batchSizeBytesInt64 - int64(buf.Len()) {
			if file.f == nil {
				// Get new file, if there are files remaining
				if !(i < len(filesIn)) {
					break
				}
				file.info = filesIn[i]
				i++
				// Open file
				var err error
				file.f, err = os.Open(file.info.Path)
				if err != nil {
					errorsChan <- fmt.Sprintf("Unable to read file at path '%s'. Error: %s. "+
						"Skipping...\n", file.info.Path, err)
					continue
				}
				if file.info.chunkSize() > uint(remainingSpaceInChunk) &&
					buf.Len() > minChunkSize &&
					file.info.chunkSize() < batchSizeBytes {
					// We don't have enough space in the current chunk to store the file, but the file (or what remains
					// of it if it was in a previous upload) is small enough not to be split anymore,
					// so we'll just close this chunk.
					break
				}
			}
			batch = append(batch, file.info)

			// Create new zip
			fw, err := zw.Create(file.info.Path)
			if err != nil {
				errorsChan <- fmt.Sprintf("Unable to write ZIP header for file '%s': %s\n", file.info.Path, err)
				return
			}

			var fileChunkSize uint
			{
				remainingSpaceInChunkAfterFile := remainingSpaceInChunk
				nextWriteSize := remainingSpaceInChunk
				// Write to the ZIP until we have no space remaining in the chunk or we have read the whole file
				for remainingSpaceInChunkAfterFile > 0 && file.f != nil {
					nbWritten, err := io.Copy(fw, io.LimitReader(file.f, nextWriteSize))
					if err != nil {
						errorsChan <- fmt.Sprintf("Unable to write file at path '%s' into Zip. Error: %s. "+
							"Aborting...\n", file.info.Path, err)
						return
					}
					fileChunkSize += uint(nbWritten)
					realTotalSizeFiles += nbWritten
					remainingSpaceInChunkAfterFile -= int64(buf.Len())
					// Add to count to remove from progress bar
					// Next write is the remaining space, or at least 1 Kb
					nextWriteSize = remainingSpaceInChunkAfterFile
					if nextWriteSize < 1_000 {
						nextWriteSize = 1_000
					}
					if fileChunkSize == file.info.chunkSize() {
						// We have read the whole file to different chunks, close it
						if err := file.f.Close(); err != nil {
							errorsChan <- fmt.Sprintf("Unable to close file '%s': %s\n", file.info.Path, err)
							return
						}

						file.f = nil
						file.info = nil
					}
				}
			}
			if file.info != nil { // A file needs to be split
				nextInfo := *file.info // Copy current info
				file.info.ByteRange.End = file.info.ByteRange.Start + fileChunkSize
				nextInfo.ByteRange.Start = file.info.ByteRange.End
				file.info = &nextInfo
				break // Move on to next chunk
			}
		}
		// Close archive
		if err := zw.Close(); err != nil {
			errorsChan <- fmt.Sprintln("Unable to close Zip archive:", err)
			return
		}

		index := chunkCounter.nextIndex()
		if len(batch) > 0 {
			b := buf.Bytes()
			// Adjust progress
			progressBarTotal <- int64(len(b)) - realTotalSizeFiles
			// Send chunk to upload
			chunk := &chunkWithBuffer{
				Info: chunkInfo{
					Index:    index,
					Files:    batch,
					Size:     uint(len(b)),
					SHA1Hash: hex.EncodeToString(hSHA.Sum(nil)),
				},
				bytes: b,
			}
			select {
			case chunksOutChan <- chunk:
			case <-ctx.Done():
				return
			}
		}
	}
}

// filesInCurrentDirectory Recursively lists all files in the current directory,
// skipping directories that should be ignored as per `dataset.ShouldIgnore`,
// and pushes the paths down the provided `chanPaths` channel.
func filesInCurrentDirectory(chanPaths chan<- string, ignoreSet map[string]bool) error {
	defer close(chanPaths) // Signal done
	defer jww.TRACE.Printf("Done going listing file paths\n\n")
	jww.TRACE.Printf("Listing file paths\n\n")
	err := godirwalk.Walk(".", &godirwalk.Options{
		Callback: func(p string, de *godirwalk.Dirent) error {
			if !de.IsDir() {
				if shouldIgnore := ignoreSet[p]; !shouldIgnore {
					chanPaths <- p
				}
			}
			return nil
		},
		Unsorted: true, // (optional) set true for faster yet non-deterministic enumeration (see godoc)
	})
	return err
}

// fileWorker Retrieves paths to files from `chanPaths`, transforms them into `fileUploadInfo`
// if the path shouldn't be ignored, and pushes the result to `chanFiles`.
func fileWorker(chanPaths <-chan string, chanFiles chan<- *fileUploadInfo, group *sync.WaitGroup,
	errChan chan<- string) {
	defer jww.TRACE.Printf("File request builder done.\n\n")
	jww.TRACE.Printf("Started file request builder...\n\n")
	for p := range chanPaths { // p is a path to a file
		if ShouldIgnore(p) {
			// The check is done here to allow multi-threaded check of globpath
			continue
		}
		if f := createFileRequest(p, errChan); f != nil {
			chanFiles <- f
		} // Otherwise, an error message was already printed by `createFileRequest`
	}
	group.Done()
}

func (cInfo *chunkInfo) buildChunkFromChunkInfo() (*chunkWithBuffer, error) {
	// Create Zip and SHA1 writers.
	var buf bytes.Buffer
	hSHA := sha1.New()
	zw := zip.NewWriter(io.MultiWriter(&buf, hSHA))
	zw.RegisterCompressor(zip.Deflate, func(out io.Writer) (io.WriteCloser, error) {
		return flate.NewWriter(out, flate.BestCompression)
	})
	for _, info := range cInfo.Files {
		err := func() error {
			// Open file
			f, err := os.Open(info.Path)
			if err != nil {
				return fmt.Errorf("Unable to read file at path '%s'. Error: %s. "+
					"Stopping...\n", info.Path, err)
			}
			defer func() {
				// Close file
				err = f.Close()
				if err != nil {
					jww.ERROR.Printf("Unable to close file at path '%s'. Error: %s. "+
						"Ignoring...\n", info.Path, err.Error())
				}
			}()

			// Seek in file
			_, err = f.Seek(int64(info.ByteRange.Start), io.SeekStart)
			if err != nil {
				return fmt.Errorf("Unable to seek in file at path '%s'. Error: %s. "+
					"Stopping...\n", info.Path, err)
			}

			// Write file in chunk
			fw, err := zw.Create(info.Path)
			if err != nil {
				return fmt.Errorf("unable to write Zip header for file '%s': %s", info.Path, err)
			}
			if _, err := io.Copy(fw, io.LimitReader(f, int64(info.chunkSize()))); err != nil {
				return fmt.Errorf("unable to write file at path '%s' into Zip. Error: %s. "+
					"Aborting... ", info.Path, err)
			}
			return nil
		}()
		if err != nil {
			_ = zw.Close()
			return nil, err
		}
	}
	err := zw.Close()
	if err != nil {
		jww.ERROR.Printf("Unable to close chunk. Error: %s. Ignoring...\n", err.Error())
	}
	b := buf.Bytes()
	return &chunkWithBuffer{
		Info: chunkInfo{
			Index:    cInfo.Index,
			Files:    cInfo.Files,
			Size:     uint(len(b)),
			SHA1Hash: hex.EncodeToString(hSHA.Sum(nil)),
		},
		bytes: b,
	}, nil
}

// isAvailableLocally Returns true if the file represented by `info` is
// available locally and matches the Size, ContentType, and MD5/SHA1 hashes.
func (info *fileUploadInfo) isAvailableLocally() bool {
	errs := make(chan string)
	var out *fileUploadInfo
	go func() {
		out = createFileRequest(info.Path, errs)
		close(errs)
	}()
	for err := range errs {
		fmt.Println(err) // Unable to find file: return error
		return false
	}
	// No error, let's compare the files
	return out != nil &&
		out.Path == info.Path &&
		out.Size == info.Size &&
		out.ContentType == info.ContentType &&
		out.MD5Hash == info.MD5Hash &&
		out.SHA1Hash == info.SHA1Hash
}

// createFileRequest Builds and returns a `fileUploadInfo` from a given path to a file.
// It is assumed that the path is not a directory.
func createFileRequest(p string, errChan chan<- string) *fileUploadInfo {
	o := &fileUploadInfo{
		Path: p,
	}
	// Open the file
	f, err := os.Open(p)
	if err != nil {
		errChan <- fmt.Sprintf("Unable to open file '%s': %s. The file was skipped...\n", p, err)
		return nil
	}
	defer f.Close()

	// Determine size
	info, err := f.Stat()
	if err != nil {
		errChan <- fmt.Sprintf("Unable to stat file '%s': %s. The file was skipped...\n", p, err)
		return nil
	}
	o.ByteRange.Start = 0
	o.ByteRange.End = uint(info.Size())
	o.Size = o.ByteRange.End

	// Content-type
	// From https://golangcode.com/get-the-content-type-of-file/
	buffer := make([]byte, 512) // Only the first 512 bytes are used to sniff the content type.
	_, err = f.Read(buffer)
	if err != nil {
		if err == io.EOF {
			jww.INFO.Printf("File at path '%s' is not big enough to detect content type, "+
				"unable to determine content-type. Uploading as is using \"application/octet-stream\".\n\n",
				p)
		} else {
			errChan <- fmt.Sprintf("Unable to read file at path '%s' to determine Content-Type: %s. The file was skipped...\n",
				p, err)
			return nil
		}
	}
	// Always returns a valid content-type by returning "application/octet-stream"
	// if no others seemed to match.
	o.ContentType = http.DetectContentType(buffer)

	// Seek back to the beginning of the file
	_, err = f.Seek(0, io.SeekStart)
	if err != nil {
		errChan <- fmt.Sprintf("Unable to seek back in file '%s': %s. The file was skipped...\n", p, err)
		return nil
	}

	// Compute hashes
	// Create Hash functions
	hMD5 := md5.New()
	hSHA := sha1.New()
	hashFuncs := io.MultiWriter(hMD5, hSHA)
	// Write to all hash functions
	if _, err := io.Copy(hashFuncs, f); err != nil {
		errChan <- fmt.Sprintf("Unable to compute MD5 hash of file '%s': %s. The file was skipped...", p, err)
		return nil
	}
	// Save hashes
	o.MD5Hash = base64.StdEncoding.EncodeToString(hMD5.Sum(nil))
	o.SHA1Hash = base64.StdEncoding.EncodeToString(hSHA.Sum(nil))

	return o
}

func signingWorker(ctx context.Context, cancel context.CancelFunc, cToUpload <-chan *chunkWithBuffer, uploadID string, uploadQueue chan<- *chunkWithBuffer, errChan chan<- string) {
	// Close the upload queue to the main goroutine
	defer jww.DEBUG.Printf("Closed upload queue.\n\n")
	defer close(uploadQueue)
	defer jww.DEBUG.Printf("Stoping signing worker.\n\n")
	jww.DEBUG.Printf("Starting signing worker.\n\n")

	// Prepare API client and set `dataset_id` parameter
	client, err := api.NewClient()
	if err != nil {
		errChan <- fmt.Sprintln("Unable to create a client for the API. Error:", err)
		cancel()
		return
	}
	client.SetPathParams(map[string]string{"dataset_id": ID(), "upload_id": uploadID})

	// Read cToUpload, aggregating them in `chunksToUpload` and
	// making a grouped sign request every 200 ms
	const aggregationWait = 200 * time.Millisecond
	jww.DEBUG.Printf("Starting signing loop\n\n")
	chunksToUpload := make([]*chunkWithBuffer, 0)
	t := time.NewTimer(0)
	if !t.Stop() {
		<-t.C
	}
	for cToUpload != nil || len(chunksToUpload) > 0 {
		select {
		case toUpload, stillOpen := <-cToUpload:
			// Aggregate chunks to `chunksToUpload`
			if !stillOpen {
				cToUpload = nil
				break
			}
			if len(chunksToUpload) == 0 {
				t.Reset(aggregationWait)
			}
			chunksToUpload = append(chunksToUpload, toUpload)
		case <-t.C:
			// Timer fired, time to signal to the server what next files we're going to upload
			chunks := make([]chunkInfo, len(chunksToUpload))
			for i, toUpload := range chunksToUpload {
				chunks[i] = toUpload.Info
			}
			if ok := signUploadRequest(client, chunks, errChan); !ok {
				err = fmt.Errorf("unable to sign chunk")
				jww.ERROR.Println(err)
				errChan <- fmt.Sprintf("%s\n", err.Error())
				cancel()
				return
			}
			jww.TRACE.Printf("Successfully signed upload request for %d chunks.\n\n", len(chunksToUpload))
			for _, toUpload := range chunksToUpload {
				select { // Send the upload to an uploader
				case uploadQueue <- toUpload: // Not stuck
				case <-ctx.Done(): // if we're stuck, check if the context was not canceled
					jww.TRACE.Printf("Signing Worker is stopping on cancel...\n\n")
					return
				}
			}
			chunksToUpload = chunksToUpload[:0]
		case <-ctx.Done():
			jww.TRACE.Printf("Signing Worker is stopping on cancel...\n\n")
			return
		}
	} // No more files, or connection is bad
}

type signRequest struct {
	ToVerify []chunkInfo `json:"chunks,omitempty"`
}

func signUploadRequest(c *resty.Client, in []chunkInfo, errChan chan<- string) bool {
	req := c.R()
	req.SetBody(signRequest{ToVerify: in}).SetError(&api.JouleErrorResponse{})
	r, err := req.Post("/dataset/{dataset_id}/upload/{upload_id}/files")
	if err != nil {
		errChan <- fmt.Sprintln("Error happened trying to sign upload request. Error:", err)
		return false
	}
	switch r.StatusCode() {
	case http.StatusOK, http.StatusNoContent:
		return true
	case http.StatusBadRequest:
		errChan <- fmt.Sprintln("The server is unable to sign an upload for some of our files. " +
			"Please make sure the dataset specification is correct.")
		return false
	default:
		sentry.ConfigureScope(func(s *sentry.Scope) {
			s.SetTag("response-code", strconv.Itoa(r.StatusCode()))
			if cloudTrace := r.Header().Get("x-cloud-trace-context"); len(cloudTrace) > 0 {
				s.SetTag("cloud-trace", cloudTrace)
			}
		})
		sentry.CaptureException(errors.New("unexpected code received when verifying files to upload"))
		errChan <- fmt.Sprintln("An unexpected code was returned. " +
			"Please make sure you are using the latest version of joule and try again later.")
		return false
	}
}

type uploadClient struct {
	*http.Client
	uploadAuthRequest
}

func newUploadClient(uploadID string) (*uploadClient, error) {
	client, err := api.NewClient()
	if err != nil {
		return nil, err
	}
	client.SetPathParams(map[string]string{"dataset_id": ID(), "upload_id": uploadID})
	resp, err := client.R().SetResult(&uploadAuthRequest{}).Get("/dataset/{dataset_id}/upload/{upload_id}")
	if err != nil {
		return nil, err
	}
	switch resp.StatusCode() {
	case http.StatusOK:
		uploadRequest, ok := resp.Result().(*uploadAuthRequest)
		if !ok || len(uploadRequest.UploadURL) == 0 {
			return nil, errors.New("unexpected server response when requesting upload URL")
		}
		return &uploadClient{
			Client:            http.DefaultClient,
			uploadAuthRequest: *uploadRequest,
		}, nil
	case http.StatusExpectationFailed, http.StatusPreconditionFailed:
		return nil, errors.New("the server reported that the upload was not started, this is a bug")
	case http.StatusConflict:
		return nil, errors.New("the server reported that another upload is already running, please try again")
	default:
		return nil, fmt.Errorf(
			"unexpected server response code (%d) when requesting upload URL", resp.StatusCode())
	}
}

func uploadWorker(ctx context.Context, cancel context.CancelFunc, uploadID string, bar *mpb.Bar, errChan chan<- string, chunksChan <-chan *chunkWithBuffer, wg *sync.WaitGroup) {
	defer wg.Done()
	defer jww.DEBUG.Printf("Upload worker is done.\n\n")
	jww.DEBUG.Printf("Starting upload worker\n\n")

	const (
		nbTries            = 10
		retryLongWaitTime  = 5 * time.Second
		retryShortWaitTime = 200 * time.Millisecond
	)

	var uClient *uploadClient
	createNewClient := func() (err error) {
		jww.INFO.Printf("Asking for a new upload client.\n\n")
		uClient, err = newUploadClient(uploadID)
		return
	}

ChunkLoop:
	for {
		select {
		case c, stillOpen := <-chunksChan:
			if !stillOpen {
				break ChunkLoop
			}
			jww.TRACE.Printf("Starting upload of chunk %d.\n\n", c.Info.Index)
			try := nbTries
			shortRetries := 0
		UploadTryLoop:
			for try > 0 {
				if uClient == nil {
					err := createNewClient()
					if err != nil {
						msg := fmt.Sprintf("Unable to negotiate a new upload client. Error: %s\n", err.Error())
						errChan <- msg
						jww.ERROR.Printf(msg)
						cancel()
						return
					}
				} // A new client was generated
				switch upload(uClient, c, bar, errChan) {
				case uploadSuccess:
					jww.DEBUG.Printf("Successfully uploaded chunk %d.\n\n", c.Info.Index)
					if try < nbTries {
						jww.INFO.Printf("Upload of chunk %d was successful after retry %d.\n",
							c.Info.Index, try)
					}
					break UploadTryLoop
				case uploadRetryWait:
					// Not a success, let's wait first...
					if try > 1 {
						jww.DEBUG.Printf("Upload of chunk %d failed. Waiting a few seconds before retry.\n\n", c.Info.Index)
						time.Sleep(retryLongWaitTime)
						jww.TRACE.Printf("Done waiting for chunk %d.\n\n", c.Info.Index)
					}
					if try == 1 {
						msg := fmt.Sprintf("Upload of chunk %d failed after %d retries. Stopping...\n", c.Info.Index, nbTries)
						errChan <- msg
						jww.ERROR.Printf(msg)
						cancel()
						return
					}
					try--
					fallthrough // ...and then fallthrough to uploadRetryNow
				case uploadRetryNow:
					uClient = nil // Invalidate the current client
					time.Sleep(retryShortWaitTime)
					shortRetries++
					jww.INFO.Printf("Retry #%d for upload of chunk %d.\n\n", shortRetries, c.Info.Index)
					if shortRetries == 100 {
						sentry.CaptureMessage("Client retried more than 100 times already.")
					}
				}
			}
		case <-ctx.Done():
			break ChunkLoop
		}
	}
}

// proxyReader is io.Reader wrapper, for proxy read bytes
type proxyReader struct {
	io.ReadCloser
	bar       *mpb.Bar
	iT        time.Time
	bytesRead int
}

func (pr *proxyReader) Read(p []byte) (n int, err error) {
	n, err = pr.ReadCloser.Read(p)
	if n > 0 {
		pr.bar.IncrBy(n, time.Since(pr.iT))
		pr.iT = time.Now()
		pr.bytesRead += n
	}
	return
}

// CancelProgress Remove all progress that was added to the
// progress bar since the beginning.
func (pr *proxyReader) CancelProgress() {
	pr.bar.IncrBy(-pr.bytesRead, time.Since(pr.iT))
	pr.iT = time.Now()
	pr.bytesRead = 0
}

// ProxyReader wraps r with metrics required for progress tracking.
func newProxyReader(r io.Reader, b *mpb.Bar) *proxyReader {
	if r == nil {
		return nil
	}
	rc, ok := r.(io.ReadCloser)
	if !ok {
		rc = ioutil.NopCloser(r)
	}
	return &proxyReader{rc, b, time.Now(), 0}
}

type uploadResultType int

const (
	uploadSuccess uploadResultType = iota
	uploadRetryNow
	uploadRetryWait
)

func upload(uClient *uploadClient, chunk *chunkWithBuffer, bar *mpb.Bar, errChan chan<- string) uploadResultType {
	var r *http.Response
	var err error
	progressReader := newProxyReader(bytes.NewReader(chunk.bytes), bar)
	defer progressReader.Close() // Absolutely useless, but here for good measure

	// Upload file
	var req *http.Request
	req, err = http.NewRequest(http.MethodPost, uClient.UploadURL, progressReader)
	if err != nil {
		msg := fmt.Sprintf("Unable to create request to upload chunk #%d. Error: %s. Skipping...\n",
			chunk.Info.Index, err)
		errChan <- msg
		jww.ERROR.Printf("%s\n", msg)
		progressReader.CancelProgress()
		return uploadRetryWait
	}
	for header, value := range uClient.Headers {
		req.Header.Set(header, value)
	}
	req.ContentLength = int64(chunk.Info.Size)
	req.Header.Set(uClient.AdditionalHeaders.PartIndex, strconv.FormatUint(chunk.Info.Index, 10))
	req.Header.Set(uClient.AdditionalHeaders.PartSHA1, chunk.Info.SHA1Hash)

	// Do request
	r, err = uClient.Client.Do(req)
	if err != nil {
		msg := fmt.Sprintf("Unable to upload chunk #%d. Error: %s. Skipping...\n",
			chunk.Info.Index, err)
		errChan <- msg
		jww.ERROR.Printf("%s\n", msg)
		progressReader.CancelProgress()
		return uploadRetryWait
	}
	defer r.Body.Close()

	// Handle response
	switch r.StatusCode {
	case http.StatusOK:
		return uploadSuccess
	case http.StatusInternalServerError, http.StatusServiceUnavailable:
		jww.DEBUG.Printf("Upload of chunk %d returned a %d error.\n\n", chunk.Info.Index, r.StatusCode)
		progressReader.CancelProgress()
		return uploadRetryNow
	default:
		bodyStr := ""
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			jww.DEBUG.Printf("Note: error reading body when parsing unexpected error code: %s.\n\n", err.Error())
		} else {
			bodyStr = string(body)
		}
		err = fmt.Errorf("unexpected status code when uploading chunk #%d. Code returned by B2: %d",
			chunk.Info.Index, r.StatusCode)
		sentry.CaptureException(err)
		jww.ERROR.Printf("%s, Body: \n%s\n\n", err.Error(), bodyStr)
		errChan <- fmt.Sprintf("Error: %s. Skipping...\n", err.Error())
		progressReader.CancelProgress()
		return uploadRetryWait
	}
}
