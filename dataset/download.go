package dataset

import (
	"archive/zip"
	"bytes"
	"context"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/vbauerster/mpb/v4"

	"gitlab.com/joule-host/joule-cli/api"
	"gitlab.com/joule-host/joule-cli/globals"
)

const (
	nbDownloaders uint = 4
	nbFileWriters uint = nbDownloaders * 50
)

// OverwriteFiles When set to true, the download will remove files prior to downloading the files
var OverwriteFiles bool

type downloadedFile struct {
	sync.Mutex
	api.DatasetFile
	f                    *os.File
	remainingChunksCount int
	chunkMapping         []*api.DatasetFileChunk
}

type indexedChunk struct {
	api.DatasetChunk
	index int
}

type downloadedChunk struct {
	indexedChunk
	buf bytes.Buffer
}

// DownloadDataset The Cobra command to download the dataset
// represented by the current configuration.
func DownloadDataset(_ *cobra.Command, _ []string) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Make sure the dataset exists on the server
	if !datasetExists(nil) {
		return globals.ErrDatasetDoesNotExist
	}

	// TODO: check that the dataset is uploaded
	fmt.Println("Fetching information from server...\n" +
		"\t(this might take some time for datasets with many files)")

	// Fetch all available files and chunks
	chunks := make([]api.DatasetChunk, 0)
	files := make([]api.DatasetFile, 0)
	{
		chunksCh, errChChunks := api.GetDatasetChunksAsync(ctx, ID())
		datasetFilesCh, errChFiles := api.GetDatasetFilesAsync(ctx, ID())
		for chunksCh != nil || errChChunks != nil ||
			errChFiles != nil || datasetFilesCh != nil {
			select {
			case chunksBatch, open := <-chunksCh:
				if !open {
					chunksCh = nil
					continue
				}
				chunks = append(chunks, chunksBatch...)
			case filesBatch, open := <-datasetFilesCh:
				if !open {
					datasetFilesCh = nil
					continue
				}
				files = append(files, filesBatch...)
			case err, open := <-errChChunks:
				if !open {
					errChChunks = nil
				}
				if err != nil {
					return err
				}
			case err, open := <-errChFiles:
				if !open {
					errChFiles = nil
				}
				if err != nil {
					return err
				}
			}
		}
	}

	fmt.Println("Preparing download...")
	// Create a file mapping of path -> file+chunk while checking the files are not on disk
	filesToDownload, total, err := buildFileToDownloadMap(files, chunks)
	if err != nil {
		return err
	}

	p, bar := createProgressBar(total)
	errChan := make(chan string)
	// Start download workers
	wg := &sync.WaitGroup{}
	chunksChan := make(chan indexedChunk)
	downloadedChunksChan := make(chan *downloadedChunk)
	for i := uint(0); i < nbDownloaders; i++ {
		wg.Add(1)
		go downloadWorker(ctx, cancel, wg, bar, chunksChan, downloadedChunksChan, errChan)
	}

	// Start file writer worker
	wgFileWriters := &sync.WaitGroup{}
	for i := uint(0); i < nbFileWriters; i++ {
		wgFileWriters.Add(1)
		go fileWriterWorker(ctx, cancel, wgFileWriters, downloadedChunksChan, filesToDownload, errChan)
	}

	// Fan out files to channel
	go func() {
		for i, c := range chunks {
			chunksChan <- indexedChunk{
				index:        i,
				DatasetChunk: c,
			}
		}
		close(chunksChan)
		// Waits for downloads to complete
		wg.Wait()
		close(downloadedChunksChan)
		// Waits for termination and closes the error channel to signal
		// that no more errors can happen.
		wgFileWriters.Wait()
		close(errChan)
	}()
	errs := make([]string, 0)
	for toShow := range errChan {
		fmt.Printf("\n%s\n\n", toShow)
		errs = append(errs, toShow)
	}

	bar.SetTotal(bar.Current(), true)
	p.Wait()

	if len(errs) > 0 {
		fmt.Println("Some errors happened during download:")
	} else {
		fmt.Println("Download is complete.")
	}
	for _, toShow := range errs {
		fmt.Print(toShow)
	}

	return nil
}

// buildFileToDownloadMap Given a list of available files, and a list of chunks,
// builds and returns a mapping of file path to `downloadedFile`
func buildFileToDownloadMap(files []api.DatasetFile, chunks []api.DatasetChunk) (map[string]*downloadedFile, int64, error) {
	var total int64 = 0
	filesToDownload := make(map[string]*downloadedFile, len(files))
	if OverwriteFiles {
		jww.INFO.Println("Removing local files...")
		fmt.Println("(Removing local files...)")
	}
	for _, file := range files {
		err := ensureFileDoesNotExist(file)
		if err != nil {
			errOut := fmt.Errorf("unable to carry on with download of file '%s': %w", file.Path, err)
			jww.ERROR.Println(errOut)
			if os.IsExist(err) {
				return nil, total, fmt.Errorf("Unable to proceed with download. "+
					"Some files ('%s' for instance) already exist locally.\n"+
					"Use flag `--force` to overwrite local files.", file.Path)
			}
			return nil, total, err
		}
		filesToDownload[file.Path] = &downloadedFile{
			DatasetFile: file,
		}
	}
	if OverwriteFiles {
		jww.INFO.Println("Done removing local files.")
	}

	nbChunks := len(chunks)
	for chunkIdx, chunk := range chunks {
		total += int64(chunk.ChunkSize)
		for i, fileChunk := range chunk.Files {
			if len(filesToDownload[fileChunk.FilePath].chunkMapping) == 0 {
				filesToDownload[fileChunk.FilePath].chunkMapping = make([]*api.DatasetFileChunk, nbChunks)
			}
			filesToDownload[fileChunk.FilePath].remainingChunksCount++
			filesToDownload[fileChunk.FilePath].chunkMapping[chunkIdx] = &chunk.Files[i]
		}
	}

	return filesToDownload, total, nil
}

func downloadWorker(ctx context.Context, cancelCtx context.CancelFunc, wg *sync.WaitGroup, bar *mpb.Bar,
	chunks <-chan indexedChunk, downloadedChunks chan<- *downloadedChunk, errChan chan<- string) {
	defer jww.DEBUG.Println("Stopped Download Worker")
	defer wg.Done()
	jww.DEBUG.Println("Spawned Download Worker")
Loop:
	for {
		select {
		case c, ok := <-chunks:
			if !ok {
				jww.INFO.Printf("Chunks loop is closed.\n\n")
				break Loop
			}
			out := &downloadedChunk{
				indexedChunk: c,
			}
			shouldContinue := downloadChunk(out, bar, errChan)
			if !shouldContinue {
				cancelCtx()
				break Loop
			}
			downloadedChunks <- out
		case <-ctx.Done():
			jww.INFO.Printf("Download worker context was cancelled. Stopping...\n\n")
			break Loop
		}
	}
	// Last loop to empty chunks
	for range chunks {
	}
}

func downloadChunk(c *downloadedChunk, bar *mpb.Bar, errChan chan<- string) (shouldContinue bool) {
	shouldContinue = true

	// Parse download URL
	// Note: this could be optional, but let's do it just to make sure.
	u, err := url.Parse(c.DownloadLinkString)
	if err != nil {
		errChan <- fmt.Sprintln("Unable to parse download URL request for chunk, error:", err)
		return false
	}

	// Create request for file
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		errChan <- fmt.Sprintln("Unable to create download request for file:", err)
		shouldContinue = false
		return
	}
	// Add headers
	for key, value := range c.DownloadHeaders {
		req.Header.Set(key, value)
	}

	// Perform request
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		jww.ERROR.Printf("Unable to download chunk with index %d. Error: %s\n\n", c.index, err.Error())
		errChan <- fmt.Sprintln("Unable to download file:", err)
		shouldContinue = false
		return
	}
	body := newProxyReader(resp.Body, bar)
	defer body.Close()

	switch resp.StatusCode {
	case http.StatusOK, http.StatusPartialContent:
		jww.DEBUG.Printf("Successfully downloaded chunk with index %d.\n\n", c.index)
		break
	case http.StatusUnauthorized:
		errChan <- fmt.Sprintln("Unable to download chunk: the download URL has incorrect authorizations.")
		// TODO: Try reauthenticating instead of quitting
		shouldContinue = false
		return
	default:
		errChan <- fmt.Sprintln("Unable to download chunk of dataset. Unexpected status code:", resp.StatusCode)
		return
	}

	// Read body by decompressing to buffer and checking SHA1
	hSHA := sha1.New()
	if _, err := io.Copy(&c.buf, io.TeeReader(body, hSHA)); err != nil {
		errChan <- fmt.Sprintln("Unable to read chunk from dataset. Error:", err)
		return
	}

	// Verify hash
	sha := hex.EncodeToString(hSHA.Sum(nil))
	if sha != c.ChunkSHA1 {
		errChan <- fmt.Sprintln("Invalid SHA1 hash for chunk. Skipping...")
		// TODO: Restart download if data is corrupt
		return
	}

	return
}

func fileWriterWorker(ctx context.Context, cancelCtx context.CancelFunc, wg *sync.WaitGroup, chunks <-chan *downloadedChunk, downloadedFiles map[string]*downloadedFile, errChan chan<- string) {
	defer wg.Done()
ChunkLoop:
	for {
		select {
		case c, ok := <-chunks:
			if !ok {
				break ChunkLoop
			}
			// GZip
			zipBytes := c.buf.Bytes()
			zr, err := zip.NewReader(bytes.NewReader(zipBytes), int64(len(zipBytes)))
			if err != nil {
				jww.ERROR.Printf(
					"Unable to create Zip Reader from downloaded chunk. Error: %s.\n\n", err.Error())
				errChan <- fmt.Sprintln("Unable read downloaded chunk as a Zip. Error:", err)
				continue ChunkLoop
			}
			for _, file := range zr.File { // Loop through files in the archive

				if d := downloadedFiles[file.Name]; len(file.Name) > 0 && d != nil {
					if shouldContinue := writeFile(d, c.index, file, errChan); !shouldContinue {
						cancelCtx()
						break ChunkLoop // We should stop
					}
				} else {
					sentry.ConfigureScope(func(s *sentry.Scope) {
						s.SetExtras(map[string]interface{}{
							"dataset_id":  ID(),
							"chunk_index": c.index,
							"file_path":   file.Name,
						})
					})
					sentry.CaptureMessage("Unexpected archive file.")
					errChan <- fmt.Sprintf("File in archive ('%s') was not supposed to be here, it was skipped.\n",
						file.Name)
				}
			}
		case <-ctx.Done():
			break ChunkLoop
		}
	}
	// Ensure chunks is emptied.
	for range chunks {
	}
}

func writeFile(downloadedFile *downloadedFile, chunkIndex int, file *zip.File, errChan chan<- string) (shouldContinue bool) {
	in, err := file.Open()
	if err != nil {
		errChan <- fmt.Sprintln("Unable to create read file from downloaded chunk. Error:", err)
		shouldContinue = false
		return
	}
	defer in.Close()

	downloadedFile.Lock()
	defer downloadedFile.Unlock()

	downloadedFile.remainingChunksCount--

	if downloadedFile.f == nil {
		// Create directory
		err := os.MkdirAll(filepath.Dir(downloadedFile.Path), 0744)
		if err != nil {
			errChan <- fmt.Sprintln("Unable to create directory '" + filepath.Dir(downloadedFile.Path) + "'. Skipping...")
			return
		}

		// Create file
		downloadedFile.f, err = os.OpenFile(downloadedFile.Path, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0640)
		if err != nil {
			errChan <- fmt.Errorf("unable to open file '%s': %w. Skipping... ",
				downloadedFile.Path, err).Error()
			shouldContinue = false
			return
		}
		// Increase file size
		err = downloadedFile.f.Truncate(int64(downloadedFile.Size))
		if err != nil {
			errChan <- fmt.Sprintln("Unable to increase size of file '" + downloadedFile.Path + "'. " +
				"Error: " + err.Error() + ". Skipping...")
			return
		}
	}
	if downloadedFile.chunkMapping[chunkIndex] == nil {
		errChan <- fmt.Sprintf("Chunk #%d was not supposed to contain file at path '%s'. Skipping...\n",
			chunkIndex, downloadedFile.Path)
		return
	}

	_, err = downloadedFile.f.Seek(int64(downloadedFile.chunkMapping[chunkIndex].ByteRangeStart), io.SeekStart)
	if err != nil {
		errChan <- fmt.Sprintf("Unable to seek in file at path '%s'. Skipping...\n",
			downloadedFile.Path)
		return
	}
	nbWritten, err := io.Copy(downloadedFile.f, in)
	if err != nil {
		errChan <- fmt.Sprintf("Cannot write all bytes for file at path '%s' (wrote %d bytes). "+
			"Error: %s\n", downloadedFile.Path, nbWritten, err)
		shouldContinue = false
		return
	}

	if downloadedFile.remainingChunksCount <= 0 {
		defer func() { // Close the file
			err := downloadedFile.f.Close()
			if err != nil {
				errChan <- fmt.Sprintf("Unable to close file at path '%s'. Error: %s\n",
					downloadedFile.Path, err)
			}
		}()
		// TODO: Check MD5 and SHA1 sum before closing
		if _, err := downloadedFile.f.Seek(0, io.SeekStart); err != nil {
			errChan <- fmt.Sprintf("Unable to seek for file at path '%s'. Error: %s\n",
				downloadedFile.Path, err)
			return true
		}
		hMD5 := md5.New()
		hSHA := sha1.New()
		hashFuncs := io.MultiWriter(hMD5, hSHA)
		// Write to all hash functions
		if _, err := io.Copy(hashFuncs, downloadedFile.f); err != nil {
			errChan <- fmt.Sprintf("Unable to compute MD5 hash of file '%s': %s. The file was skipped...",
				downloadedFile.Path, err)
			return false
		}
		// Save hashes
		if MD5Hash := base64.StdEncoding.EncodeToString(hMD5.Sum(nil)); MD5Hash != downloadedFile.MD5Hash {
			errChan <- fmt.Sprintf("Invalid MD5 hash for file at path '%s'. \n"+
				"\tExpected: '%s', \n"+
				"\tGot:      '%s'. \n\tThe file was skipped...",
				downloadedFile.Path, downloadedFile.MD5Hash, MD5Hash)
			return true
		}
		if SHA1Hash := base64.StdEncoding.EncodeToString(hSHA.Sum(nil)); SHA1Hash != downloadedFile.SHA1Hash {
			errChan <- fmt.Sprintf("Invalid SHA hash for file at path '%s'. \n"+
				"\tExpected: '%s', \n"+
				"\tGot:      '%s'. \n\t(This is weird since the MD5 Hashes matched) The file was skipped...",
				downloadedFile.Path, downloadedFile.SHA1Hash, SHA1Hash)
			return true
		}
	}

	return true
}

func ensureFileDoesNotExist(file api.DatasetFile) error {
	if OverwriteFiles {
		// Try to remove the file
		err := os.Remove(file.Path)
		if err == nil || os.IsNotExist(err) {
			return nil // The file was removed or did not exist
		}
		// err != nil but the File exists
		return err
	}
	// Try to look up the file
	_, err := os.Stat(file.Path)
	if err == nil {
		// The file exists
		return os.ErrExist
	}
	if os.IsNotExist(err) {
		return nil // The file does not exist
	}
	return err // err != nil but the File exists
}

// proxyReader is io.Reader wrapper, for proxy read bytes
type proxyWriter struct {
	io.Writer
	bar *mpb.Bar
	iT  time.Time
}

func (pr *proxyWriter) Write(p []byte) (n int, err error) {
	n, err = pr.Writer.Write(p)
	if n > 0 {
		pr.bar.IncrBy(n, time.Since(pr.iT))
		pr.iT = time.Now()
	}
	return
}

// ProxyReader wraps r with metrics required for progress tracking.
func newProxyWriter(w io.Writer, b *mpb.Bar) io.Writer {
	if w == nil {
		return nil
	}
	return &proxyWriter{w, b, time.Now()}
}
