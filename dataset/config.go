package dataset

import (
	"log"

	"github.com/danwakefield/fnmatch"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"

	"gitlab.com/joule-host/joule-cli/config"
)

const (
	// ConfigVersion The Spec version of the configuration
	ConfigVersion         = 1
	configName            = ".joule-spec"
	configExt             = "yaml"
	datasetConfigFilename = configName + "." + configExt
)

var defaultIgnores = []string{datasetConfigFilename, config.JouleConfigFilename, ".gitignore", ".DS_Store", "~*", ".*"}

var localV = viper.New()
var c localConfig

func init() {
	// Set up local configuration
	localV.SetConfigName(configName)
	localV.SetConfigType(configExt)
	localV.AddConfigPath(".")

	localV.SetDefault("version", ConfigVersion)
	localV.SetDefault("ignore", defaultIgnores)

	if err := localV.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Local config file not found. Set path
			localV.SetConfigFile(datasetConfigFilename)
		} else {
			// Local config file was found but another error was produced
			log.Panicln("Error reading config file:", err)
		}
	}
	if err := localV.Unmarshal(&c); err != nil {
		log.Println("Local configuration for dataset is invalid:", err)
	}
}

// T Type of dataset
type T string

const (
	typeNone  T = ""
	typeJoule T = "joule" // Represents a dataset hosted on joule.host
)

type localConfig struct {
	Version        uint     `mapstructure:"version"`
	IgnorePatterns []string `mapstructure:"ignore"`
	Dataset        struct {
		ID   string `mapstructure:"id"`   // The unique ID of the dataset
		Type T      `mapstructure:"type"` // The hosting type of the dataset. For now, only `TypeJoule` is supported.
	} `mapstructure:"dataset"`
}

// Version of the dataset configuration spec
func Version() uint {
	return c.Version
}

// ID of the dataset currently represented by the dataset configuration file
func ID() string {
	return c.Dataset.ID
}

// Type of the dataset currently represented by the dataset configuration file
func Type() T {
	return c.Dataset.Type
}

// SetVersion Set the version of the dataset configuration.
func SetVersion(v uint) <-chan struct{} {
	return set("version", v)
}

// setDataset Set the ID of the dataset in the configuration.
func setDataset(datasetID string, datasetType T) <-chan struct{} {
	return setMultiple(map[string]interface{}{
		"dataset.id":   datasetID,
		"dataset.type": datasetType,
	})
}

// ShouldIgnore Returns True if path `p` should be ignored given the
// IgnorePatters of the current configuration.
func ShouldIgnore(p string) bool {
	for _, pattern := range c.IgnorePatterns {
		if fnmatch.Match(pattern, p, 0) {
			return true
		}
	}
	return false
}

func set(k string, v interface{}) <-chan struct{} {
	localV.Set(k, v)
	return save()
}

func setMultiple(d map[string]interface{}) <-chan struct{} {
	for k, v := range d {
		localV.Set(k, v)
	}
	return save()
}

func save() <-chan struct{} {
	// Save file
	done := make(chan struct{})
	go func() {
		defer close(done)
		if len(localV.ConfigFileUsed()) == 0 {
			jww.ERROR.Println("Error: no config file path set.")
			return
		}
		err := localV.WriteConfig()
		if err != nil {
			jww.ERROR.Println("Error saving settings:", err)
			return
		}
	}()
	// Reload config
	if err := localV.Unmarshal(&c); err != nil {
		jww.ERROR.Println("Local configuration for dataset is invalid:", err)
	}
	return done
}
